const prefix = "https://collections.newberry.org/API/search/v3.0/search?query=New.Format:Postcards AND "
const suffix = "&fields=CoreField.Identifier,Document.CortexShareLink,CoreField.IIIFResourceType,Document.IIIFV3ID,Title,RelatedValue.Original-Filename,CoreField.OriginalFileName,CoreField.DocSubType&format=json"

// https://collections.newberry.org/API/search/v3.0/search?query=New.Format:postcard AND Text:bikes&fields=CoreField.Identifier,Document.CortexShareLink,CoreField.IIIFResourceType,Document.IIIFV3ID,Title&format=json

const hardlinkList = ['cats', 'bikes']
import {
    copyright,
    language,
    format,
    subject,
    place
} from '$data/filters'

const allFilters = {
    'New.Rights-Status': copyright,
    'New.Language': language,
    'New.Format': format,
    'New.Subject': subject,
    'New.Place': place
}
export async function load({
    fetch,
    params
}) {
    let metadataField = Object.keys(allFilters).filter(k => allFilters[k].some(af => af.text.toLowerCase() === params.subcat.toLowerCase())).map(f => f + ':' + params.subcat).join(' AND ')
    let items, dataload
    // console.log("params.subcat", params.subcat)
    if (hardlinkList.includes(params.subcat)) {
        const filePromise = import( /* @vite-ignore */ `../../data/${params.subcat}.json`);
        const prom = await filePromise;
        dataload = await prom.default;
    } else {
        let url = prefix + metadataField + suffix
        console.log("url", url)
        const res = await fetch(url);
        dataload = await res.json();

    }
    console.log("dataload",dataload)

    if (dataload.APIResponse.GlobalInfo.Errors){
        console.log(dataload.APIResponse.GlobalInfo.Errors)
    }
    items = await Promise.all(dataload.APIResponse.Items.map(async (item) => {
        let imggettterurl = 'https://collections.newberry.org/API/AssetLink/V1.0/CreateFormatLink?Identifier=' + item['CoreField.Identifier'] + '&CreateDownloadLink=0&StickToCurrentVersion=1&AssetFormat=TR4&LogAccess=0&token=CortexFJe0gKwZyhtx@JxqwGVQWVLl@x0oBJPC3FAFYJ6pfE4*&format=json'
        item.imageLink = await fetch(imggettterurl).then(alf => alf.json()).then(alf => {
            return alf.APIResponse.Response.Link
        })
        // console.log("item.imageLink: ", item.imageLink)
        return item

    }));
    // console.log("items: ", items)
    return {
        items
    };
}