const prefix = "https://collections.newberry.org/API/search/v3.0/search?query=RelatedValue.Format:"
const suffix = "&fields=CoreField.Identifier,Document.CortexShareLink,CoreField.IIIFResourceType,Document.IIIFV3ID,Title,RelatedValue.Original-Filename,CoreField.OriginalFileName,CoreField.DocSubType&format=json"

// Maps OR Maps (Documents) OR Cartographic materials OR Maps--Early works to 1800 OR Nautical charts OR Road maps OR Atlases OR Views (Visual works) OR Aeronautical charts OR Portolan charts OR Manuscript maps OR Gores (Maps) OR Plats (Maps)
// https://collections.newberry.org/API/search/v3.0/search?query=New.Format:postcard AND Text:bikes&fields=CoreField.Identifier,Document.CortexShareLink,CoreField.IIIFResourceType,Document.IIIFV3ID,Title&format=json

const hardlinkList = {cats: 'cats', bikes: ['bikes', 'bicycles & tricycles', 'cycling']}
import {
    copyright,
    language,
    subject,
    place
} from '$data/filters'

const filters = {
    copyright: 'RelatedValue.Rights-Status',
    language: 'RelatedValue.Language',
    subject: 'RelatedValue.Subject',
    place: 'RelatedValue.Place',
}

const formats = {
    maps: "Maps",
    postcards: 'Postcards',
    diaries: 'Diaries',
    letters: 'Correspondence',
    photos: 'Photographs'
}

export async function load({
    fetch,
    params
}) {
    let format = formats[params.doctype]
    // let metadataField = Object.keys(allFilters).filter(k => allFilters[k].some(af => af.text.toLowerCase() === params.keyword.toLowerCase())).map(f => f + ':' + params.keyword).join(' AND ')
    let metadataField = filters[params.filter]
    let items, dataload
    // console.log("params.keyword", params.keyword)
    if (hardlinkList.includes(params.keyword)) {
        const filePromise = import( /* @vite-ignore */ `../../../data/${params.keyword}.json`);
        const prom = await filePromise;
        dataload = await prom.default;
    } else {
        let url = prefix + format + " AND " + metadataField + suffix
        console.log("url", url)
        const res = await fetch(url);
        dataload = await res.json();

    }
    console.log("dataload", dataload)

    if (dataload.APIResponse.GlobalInfo.Errors) {
        console.log(dataload.APIResponse.GlobalInfo.Errors)
    }
    items = await Promise.all(dataload.APIResponse.Items.map(async (item) => {
        let imggettterurl = 'https://collections.newberry.org/API/AssetLink/V1.0/CreateFormatLink?Identifier=' + item['CoreField.Identifier'] + '&CreateDownloadLink=0&StickToCurrentVersion=1&AssetFormat=TR4&LogAccess=0&token=CortexFJe0gKwZyhtx@JxqwGVQWVLl@x0oBJPC3FAFYJ6pfE4*&format=json'
        item.imageLink = await fetch(imggettterurl).then(alf => alf.json()).then(alf => {
            return alf.APIResponse.Response.Link
        })
        // console.log("item.imageLink: ", item.imageLink)
        return item

    }));
    // console.log("items: ", items)
    return {
        items
    };
}