export const copyright = [{
    text: "Copyright Not Evaluated",
    href: "#&00A9;/idk",
    description: "Copyright"
}, {
    text: "No Copyright - United States",
    href: "#&00A9;/none",
    description: "Copyright"
}, {
    text: "In Copyright",
    href: "#&00A9;/yes",
    description: "Copyright"
}]

export const language = [
    {
        text: "English",
        href: "language/english",
        description: "Language"
    },
    {
        text: "Japanese",
        href: "language/japanese",
        description: "Language"
    },
    {
        text: "French",
        href: "language/french",
        description: "Language"
    },
    {
        text: "German",
        href: "language/german",
        description: "Language"
    },
    {
        text: "Dutch",
        href: "language/dutch",
        description: "Language"
    },
    {
        text: "Hungarian",
        href: "language/hungarian",
        description: "Language"
    },
    {
        text: "Swedish",
        href: "language/swedish",
        description: "Language"
    },
    {
        text: "Italian",
        href: "language/italian",
        description: "Language"
    },
    {
        text: "Spanish",
        href: "language/spanish",
        description: "Language"
    },
    {
        text: "Czech",
        href: "language/czech",
        description: "Language"
    },
    {
        text: "Norwegian",
        href: "language/norwegian",
        description: "Language"
    },
    {
        text: "Serbian",
        href: "language/serbian",
        description: "Language"
    },
    {
        text: "Danish",
        href: "language/danish",
        description: "Language"
    },
    {
        text: "Slovenian",
        href: "language/slovenian",
        description: "Language"
    },
]

export const format = [

    {
        text: "Postcards",
        href: "format/postcards",
        description: "Format"
    },
    {
        text: "Oilettes",
        href: "format/oilettes",
        description: "Format"
    },
    {
        text: "Pictorial works",
        href: "format/pictorial_works",
        description: "Format"
    },
    {
        text: "Printed ephemera",
        href: "format/printed_ephemera",
        description: "Format"
    },
    {
        text: "Ephemera",
        href: "format/ephemera",
        description: "Format"
    },
    {
        text: "Souvenirs",
        href: "format/souvenirs",
        description: "Format"
    },
    {
        text: "Postcard albums",
        href: "format/postcard_albums",
        description: "Format"
    },
    {
        text: "Photographs",
        href: "format/photographs",
        description: "Format"
    },
    {
        text: "Caricatures and cartoons",
        href: "format/caricatures_and_cartoons",
        description: "Format"
    },
    {
        text: "Photographic postcards",
        href: "format/photographic_postcards",
        description: "Format"
    },
    {
        text: "Broadsides",
        href: "format/broadsides",
        description: "Format"
    },
    {
        text: "Genealogy",
        href: "format/genealogy",
        description: "Format"
    },
    {
        text: "Birth certificates",
        href: "format/birth_certificates",
        description: "Format"
    },
    {
        text: "Negatives",
        href: "format/negatives",
        description: "Format"
    },
    {
        text: "Stereographs",
        href: "format/stereographs",
        description: "Format"
    },
    {
        text: "Greeting cards",
        href: "format/greeting_cards",
        description: "Format"
    },
    {
        text: "Cabinet photographs",
        href: "format/cabinet_photographs",
        description: "Format"
    },
    {
        text: "Maps",
        href: "format/maps",
        description: "Format"
    },
    {
        text: "Correspondence",
        href: "format/correspondence",
        description: "Format"
    },
    {
        text: "Valentines",
        href: "format/valentines",
        description: "Format"
    },
    {
        text: "Phonograph records",
        href: "format/phonograph_records",
        description: "Format"
    },
    {
        text: "Glass plate negatives",
        href: "format/glass_plate_negatives",
        description: "Format"
    },
    {
        text: "Lectures",
        href: "format/lectures",
        description: "Format"
    },
    {
        text: "Portraits",
        href: "format/portraits",
        description: "Format"
    },
    {
        text: "Cartes-de-visite",
        href: "format/cartesdevisite",
        description: "Format"
    },
    {
        text: "Open reel audiotapes",
        href: "format/open_reel_audiotapes",
        description: "Format"
    },
    {
        text: "Periodicals",
        href: "format/periodicals",
        description: "Format"
    },
    {
        text: "Lantern slides",
        href: "format/lantern_slides",
        description: "Format"
    },
    {
        text: "Slides",
        href: "format/slides",
        description: "Format"
    },
    {
        text: "Mini-DV",
        href: "format/minidv",
        description: "Format"
    },
    {
        text: "Videotapes",
        href: "format/videotapes",
        description: "Format"
    },
    {
        text: "DVDs",
        href: "format/dvds",
        description: "Format"
    },
    {
        text: "Cartographic materials",
        href: "format/cartographic_materials",
        description: "Format"
    },
    {
        text: "Audiocassettes",
        href: "format/audiocassettes",
        description: "Format"
    },
    {
        text: "Maps (Documents)",
        href: "format/maps",
        description: "Format"
    },
    {
        text: "Drawings",
        href: "format/drawings",
        description: "Format"
    },
    {
        text: "Diaries",
        href: "format/diaries",
        description: "Format"
    },
    {
        text: "Motion pictures (Visual works)",
        href: "format/motion_pictures",
        description: "Format"
    },
    {
        text: "Lithographs",
        href: "format/lithographs",
        description: "Format"
    },
    {
        text: "Watercolor drawings",
        href: "format/watercolor_drawings",
        description: "Format"
    },
    {
        text: "Ink drawings",
        href: "format/ink_drawings",
        description: "Format"
    },
    {
        text: "Maps--Early works to 1800",
        href: "format/mapsearly_works_to_1800",
        description: "Format"
    },
    {
        text: "Digital exhibitions",
        href: "format/digital_exhibitions",
        description: "Format"
    },
    {
        text: "Early works to 1800",
        href: "format/early_works_to_1800",
        description: "Format"
    },
    {
        text: "Scrapbooks",
        href: "format/scrapbooks",
        description: "Format"
    },
    {
        text: "Nautical charts",
        href: "format/nautical_charts",
        description: "Format"
    },
    {
        text: "Clippings",
        href: "format/clippings",
        description: "Format"
    },
    {
        text: "Tape reels",
        href: "format/tape_reels",
        description: "Format"
    },
    {
        text: "Views (Visual works)",
        href: "format/views",
        description: "Format"
    },
    {
        text: "Treaties",
        href: "format/treaties",
        description: "Format"
    },
    {
        text: "Wood engravings",
        href: "format/wood_engravings",
        description: "Format"
    },
    {
        text: "History",
        href: "format/history",
        description: "Format"
    },
    {
        text: "Aeronautical charts",
        href: "format/aeronautical_charts",
        description: "Format"
    },
    {
        text: "Sound recordings",
        href: "format/sound_recordings",
        description: "Format"
    },
    {
        text: "Moving images",
        href: "format/moving_images",
        description: "Format"
    },
    {
        text: "Trade cards",
        href: "format/trade_cards",
        description: "Format"
    },
    {
        text: "Archives",
        href: "format/archives",
        description: "Format"
    },
    {
        text: "Atlases",
        href: "format/atlases",
        description: "Format"
    },
    {
        text: "Pictorial works--Early works to 1800",
        href: "format/pictorial_worksearly_works_to_1800",
        description: "Format"
    },
    {
        text: "Sermons--Early works to 1800",
        href: "format/sermonsearly_works_to_1800",
        description: "Format"
    },
    {
        text: "Records and correspondence",
        href: "format/records_and_correspondence",
        description: "Format"
    },
    {
        text: "Travel diaries",
        href: "format/travel_diaries",
        description: "Format"
    },
    {
        text: "Fiction",
        href: "format/fiction",
        description: "Format"
    },
    {
        text: "Scores",
        href: "format/scores",
        description: "Format"
    },
    {
        text: "Photographic prints",
        href: "format/photographic_prints",
        description: "Format"
    },
    {
        text: "Guidebooks",
        href: "format/guidebooks",
        description: "Format"
    },
    {
        text: "Sources",
        href: "format/sources",
        description: "Format"
    },
    {
        text: "Bird's-eye views",
        href: "format/birdseye_views",
        description: "Format"
    },
    {
        text: "Specimens",
        href: "format/specimens",
        description: "Format"
    },
    {
        text: "Etchings",
        href: "format/etchings",
        description: "Format"
    },
    {
        text: "Aerial views",
        href: "format/aerial_views",
        description: "Format"
    },
    {
        text: "Songs and music",
        href: "format/songs_and_music",
        description: "Format"
    },
    {
        text: "Engravings",
        href: "format/engravings",
        description: "Format"
    },
    {
        text: "Portrait photographs",
        href: "format/portrait_photographs",
        description: "Format"
    },
    {
        text: "Compact discs",
        href: "format/compact_discs",
        description: "Format"
    },
    {
        text: "Blueprints",
        href: "format/blueprints",
        description: "Format"
    },
    {
        text: "Schedules",
        href: "format/schedules",
        description: "Format"
    },
    {
        text: "Picture postcards",
        href: "format/picture_postcards",
        description: "Format"
    },
    {
        text: "Journals (Accounts)",
        href: "format/journals",
        description: "Format"
    },
    {
        text: "Travel guidebooks",
        href: "format/travel_guidebooks",
        description: "Format"
    },
    {
        text: "Manuscripts",
        href: "format/manuscripts",
        description: "Format"
    },
    {
        text: "Prints",
        href: "format/prints",
        description: "Format"
    },
    {
        text: "Specifications",
        href: "format/specifications",
        description: "Format"
    },
    {
        text: "Travel literature",
        href: "format/travel_literature",
        description: "Format"
    },
    {
        text: "Ration books",
        href: "format/ration_books",
        description: "Format"
    },
    {
        text: "Wall maps",
        href: "format/wall_maps",
        description: "Format"
    },
    {
        text: "Illustrations",
        href: "format/illustrations",
        description: "Format"
    },
    {
        text: "Technical drawings",
        href: "format/technical_drawings",
        description: "Format"
    },
    {
        text: "Advertisements",
        href: "format/advertisements",
        description: "Format"
    },
    {
        text: "Line photomechanical prints",
        href: "format/line_photomechanical_prints",
        description: "Format"
    },
    {
        text: "Calligraphy (visual works)",
        href: "format/calligraphy",
        description: "Format"
    },
    {
        text: "Pamphlets",
        href: "format/pamphlets",
        description: "Format"
    },
    {
        text: "Caricatures",
        href: "format/caricatures",
        description: "Format"
    },
    {
        text: "Oil paintings",
        href: "format/oil_paintings",
        description: "Format"
    },
    {
        text: "Watercolors",
        href: "format/watercolors",
        description: "Format"
    },
    {
        text: "Artifacts (Object genre)",
        href: "format/artifacts",
        description: "Format"
    },
    {
        text: "Songbooks",
        href: "format/songbooks",
        description: "Format"
    },
    {
        text: "Autographs (Manuscripts)",
        href: "format/autographs",
        description: "Format"
    },
    {
        text: "Stipple engravings",
        href: "format/stipple_engravings",
        description: "Format"
    },
    {
        text: "In art",
        href: "format/in_art",
        description: "Format"
    },
    {
        text: "Early maps",
        href: "format/early_maps",
        description: "Format"
    },
    {
        text: "Forgery of manuscripts",
        href: "format/forgery_of_manuscripts",
        description: "Format"
    },
    {
        text: "Disposable cups",
        href: "format/disposable_cups",
        description: "Format"
    },
    {
        text: "Road maps",
        href: "format/road_maps",
        description: "Format"
    },
    {
        text: "Manuscripts (Documents)",
        href: "format/manuscripts",
        description: "Format"
    },
    {
        text: "Toy and movable books",
        href: "format/toy_and_movable_books",
        description: "Format"
    },
    {
        text: "Scores--Early works to 1800",
        href: "format/scoresearly_works_to_1800",
        description: "Format"
    },
    {
        text: "Atlas factice",
        href: "format/atlas_factice",
        description: "Format"
    },
    {
        text: "Ledger drawings",
        href: "format/ledger_drawings",
        description: "Format"
    },
    {
        text: "Pop-up books",
        href: "format/popup_books",
        description: "Format"
    },
    {
        text: "Labels",
        href: "format/labels",
        description: "Format"
    },
    {
        text: "Songs and music--Early works to 1800",
        href: "format/songs_and_musicearly_works_to_1800",
        description: "Format"
    },
    {
        text: "Volvelles",
        href: "format/volvelles",
        description: "Format"
    },
    {
        text: "Boxes",
        href: "format/boxes",
        description: "Format"
    },
    {
        text: "Woodcuts",
        href: "format/woodcuts",
        description: "Format"
    },
    {
        text: "Directories",
        href: "format/directories",
        description: "Format"
    },
    {
        text: "Parts",
        href: "format/parts",
        description: "Format"
    },
    {
        text: "Personal narratives",
        href: "format/personal_narratives",
        description: "Format"
    },
    {
        text: "Chants",
        href: "format/chants",
        description: "Format"
    },
    {
        text: "Tours",
        href: "format/tours",
        description: "Format"
    },
    {
        text: "Toy theaters",
        href: "format/toy_theaters",
        description: "Format"
    },
    {
        text: "Juvenile literature",
        href: "format/juvenile_literature",
        description: "Format"
    },
    {
        text: "Controversial literature",
        href: "format/controversial_literature",
        description: "Format"
    },
    {
        text: "Census, 1890",
        href: "format/census_1890",
        description: "Format"
    },
    {
        text: "Gores (Maps)",
        href: "format/gores",
        description: "Format"
    },
    {
        text: "Plans (Maps)",
        href: "format/plans",
        description: "Format"
    },
    {
        text: "Observations",
        href: "format/observations",
        description: "Format"
    },
    {
        text: "World maps",
        href: "format/world_maps",
        description: "Format"
    },
    {
        text: "Trials, litigation, etc.",
        href: "format/trials_litigation_etc.",
        description: "Format"
    },
    {
        text: "Color printing (Printing)",
        href: "format/color_printing",
        description: "Format"
    },
    {
        text: "Surveys",
        href: "format/surveys",
        description: "Format"
    },
    {
        text: "Patriotic envelopes",
        href: "format/patriotic_envelopes",
        description: "Format"
    },
    {
        text: "Pictorial lettersheets",
        href: "format/pictorial_lettersheets",
        description: "Format"
    },
    {
        text: "Liturgical books",
        href: "format/liturgical_books",
        description: "Format"
    },
    {
        text: "Service books (Music)",
        href: "format/service_books",
        description: "Format"
    },
    {
        text: "Graduals",
        href: "format/graduals",
        description: "Format"
    },
    {
        text: "Playing cards",
        href: "format/playing_cards",
        description: "Format"
    },
    {
        text: "Plats (Maps)",
        href: "format/plats",
        description: "Format"
    },
    {
        text: "Portrait paintings",
        href: "format/portrait_paintings",
        description: "Format"
    },
    {
        text: "Realia",
        href: "format/realia",
        description: "Format"
    },
    {
        text: "Stereoscopes",
        href: "format/stereoscopes",
        description: "Format"
    },
    {
        text: "Tissue stereographs",
        href: "format/tissue_stereographs",
        description: "Format"
    },
    {
        text: "Proofs (Printing)",
        href: "format/proofs",
        description: "Format"
    },
    {
        text: "Mechanical works",
        href: "format/mechanical_works",
        description: "Format"
    }
]

export const subject = [

    {
        text: "Panama-Pacific International Exposition",
        href: "subject/panamapacific_international_exposition",
        description: "Subject"
    },
    {
        text: "Louisiana Purchase Exposition",
        href: "subject/louisiana_purchase_exposition",
        description: "Subject"
    },
    {
        text: "Exposition universelle",
        href: "subject/exposition_universelle",
        description: "Subject"
    },
    {
        text: "Exposition universelle et internationale",
        href: "subject/exposition_universelle_et_internationale",
        description: "Subject"
    },
    {
        text: "Century of Progress International Exposition",
        href: "subject/century_of_progress_international_exposition",
        description: "Subject"
    },
    {
        text: "United States Highway 66",
        href: "subject/united_states_highway_66",
        description: "Subject"
    },
    {
        text: "Fruit",
        href: "subject/fruit",
        description: "Subject"
    },
    {
        text: "Vegetables",
        href: "subject/vegetables",
        description: "Subject"
    },
    {
        text: "Folklore",
        href: "subject/folklore",
        description: "Subject"
    },
    {
        text: "Fishes",
        href: "subject/fishes",
        description: "Subject"
    },
    {
        text: "Baseball",
        href: "subject/baseball",
        description: "Subject"
    },
    {
        text: "Golf",
        href: "subject/golf",
        description: "Subject"
    },
    {
        text: "Rivers",
        href: "subject/rivers",
        description: "Subject"
    },
    {
        text: "Cities and towns",
        href: "subject/cities_and_towns",
        description: "Subject"
    },
    {
        text: "Boats",
        href: "subject/boats",
        description: "Subject"
    },
    {
        text: "Streets",
        href: "subject/streets",
        description: "Subject"
    },
    {
        text: "Children",
        href: "subject/children",
        description: "Subject"
    },
    {
        text: "Soccer",
        href: "subject/soccer",
        description: "Subject"
    },
    {
        text: "Dwellings",
        href: "subject/dwellings",
        description: "Subject"
    },
    {
        text: "Horse racing",
        href: "subject/horse_racing",
        description: "Subject"
    },
    {
        text: "Flowers",
        href: "subject/flowers",
        description: "Subject"
    },
    {
        text: "Lakes",
        href: "subject/lakes",
        description: "Subject"
    },
    {
        text: "Fishing",
        href: "subject/fishing",
        description: "Subject"
    },
    {
        text: "Mountains",
        href: "subject/mountains",
        description: "Subject"
    },
    {
        text: "Greeting cards",
        href: "subject/greeting_cards",
        description: "Subject"
    },
    {
        text: "Castles",
        href: "subject/castles",
        description: "Subject"
    },
    {
        text: "Birds",
        href: "subject/birds",
        description: "Subject"
    },
    {
        text: "Views",
        href: "subject/views",
        description: "Subject"
    },
    {
        text: "Bridges",
        href: "subject/bridges",
        description: "Subject"
    },
    {
        text: "Churches",
        href: "subject/churches",
        description: "Subject"
    },
    {
        text: "Cycling",
        href: "subject/cycling",
        description: "Subject"
    },
    {
        text: "Trees",
        href: "subject/trees",
        description: "Subject"
    },
    {
        text: "Beaches",
        href: "subject/beaches",
        description: "Subject"
    },
    {
        text: "Montages",
        href: "subject/montages",
        description: "Subject"
    },
    {
        text: "Tall tales",
        href: "subject/tall_tales",
        description: "Subject"
    },
    {
        text: "Conjectural works",
        href: "subject/conjectural_works",
        description: "Subject"
    },
    {
        text: "Trails",
        href: "subject/trails",
        description: "Subject"
    },
    {
        text: "Exhibitions",
        href: "subject/exhibitions",
        description: "Subject"
    },
    {
        text: "Hunting",
        href: "subject/hunting",
        description: "Subject"
    },
    {
        text: "Rural areas",
        href: "subject/rural_areas",
        description: "Subject"
    },
    {
        text: "Gardens",
        href: "subject/gardens",
        description: "Subject"
    },
    {
        text: "Skating",
        href: "subject/skating",
        description: "Subject"
    },
    {
        text: "Tennis",
        href: "subject/tennis",
        description: "Subject"
    },
    {
        text: "Ukiyoe",
        href: "subject/ukiyoe",
        description: "Subject"
    },
    {
        text: "Japanese",
        href: "subject/japanese",
        description: "Subject"
    },
    {
        text: "Bowling",
        href: "subject/bowling",
        description: "Subject"
    },
    {
        text: "Swimming",
        href: "subject/swimming",
        description: "Subject"
    },
    {
        text: "Forests and forestry",
        href: "subject/forests_and_forestry",
        description: "Subject"
    },
    {
        text: "Cityscapes",
        href: "subject/cityscapes",
        description: "Subject"
    },
    {
        text: "Roller skating",
        href: "subject/roller_skating",
        description: "Subject"
    },
    {
        text: "Villages",
        href: "subject/villages",
        description: "Subject"
    },
    {
        text: "Bays",
        href: "subject/bays",
        description: "Subject"
    },
    {
        text: "Ocean",
        href: "subject/ocean",
        description: "Subject"
    },
    {
        text: "Boxing",
        href: "subject/boxing",
        description: "Subject"
    },
    {
        text: "Billiards",
        href: "subject/billiards",
        description: "Subject"
    },
    {
        text: "Boats and boating",
        href: "subject/boats_and_boating",
        description: "Subject"
    },
    {
        text: "Football",
        href: "subject/football",
        description: "Subject"
    },
    {
        text: "Automobile racing",
        href: "subject/automobile_racing",
        description: "Subject"
    },
    {
        text: "Pedestrians",
        href: "subject/pedestrians",
        description: "Subject"
    },
    {
        text: "Country life",
        href: "subject/country_life",
        description: "Subject"
    },
    {
        text: "Cricket",
        href: "subject/cricket",
        description: "Subject"
    },
    {
        text: "Seascapes",
        href: "subject/seascapes",
        description: "Subject"
    },
    {
        text: "Men",
        href: "subject/men",
        description: "Subject"
    },
    {
        text: "Parks",
        href: "subject/parks",
        description: "Subject"
    },
    {
        text: "Harbors",
        href: "subject/harbors",
        description: "Subject"
    },
    {
        text: "Coasts",
        href: "subject/coasts",
        description: "Subject"
    },
    {
        text: "Couples",
        href: "subject/couples",
        description: "Subject"
    },
    {
        text: "Cliffs",
        href: "subject/cliffs",
        description: "Subject"
    },
    {
        text: "Gymnastics",
        href: "subject/gymnastics",
        description: "Subject"
    },
    {
        text: "Caricatures",
        href: "subject/caricatures",
        description: "Subject"
    },
    {
        text: "Horses",
        href: "subject/horses",
        description: "Subject"
    },
    {
        text: "Carriages and carts",
        href: "subject/carriages_and_carts",
        description: "Subject"
    },
    {
        text: "Sun--Rising and setting",
        href: "subject/sunrising_and_setting",
        description: "Subject"
    },
    {
        text: "Holidays",
        href: "subject/holidays",
        description: "Subject"
    },
    {
        text: "Roads",
        href: "subject/roads",
        description: "Subject"
    },
    {
        text: "Crowds",
        href: "subject/crowds",
        description: "Subject"
    },
    {
        text: "Towers",
        href: "subject/towers",
        description: "Subject"
    },
    {
        text: "Snow",
        href: "subject/snow",
        description: "Subject"
    },
    {
        text: "Piers",
        href: "subject/piers",
        description: "Subject"
    },
    {
        text: "Easter",
        href: "subject/easter",
        description: "Subject"
    },
    {
        text: "Christmas",
        href: "subject/christmas",
        description: "Subject"
    },
    {
        text: "Santa Claus (Fictitious character)",
        href: "subject/santa_claus",
        description: "Subject"
    },
    {
        text: "Sailboats",
        href: "subject/sailboats",
        description: "Subject"
    },
    {
        text: "Sheep",
        href: "subject/sheep",
        description: "Subject"
    },
    {
        text: "Waterfalls",
        href: "subject/waterfalls",
        description: "Subject"
    },
    {
        text: "Cows",
        href: "subject/cows",
        description: "Subject"
    },
    {
        text: "Markets",
        href: "subject/markets",
        description: "Subject"
    },
    {
        text: "Hotels",
        href: "subject/hotels",
        description: "Subject"
    },
    {
        text: "Dogs",
        href: "subject/dogs",
        description: "Subject"
    },
    {
        text: "Valleys",
        href: "subject/valleys",
        description: "Subject"
    },
    {
        text: "Meadows",
        href: "subject/meadows",
        description: "Subject"
    },
    {
        text: "Christmas cards",
        href: "subject/christmas_cards",
        description: "Subject"
    },
    {
        text: "Ruins",
        href: "subject/ruins",
        description: "Subject"
    },
    {
        text: "Gates",
        href: "subject/gates",
        description: "Subject"
    },
    {
        text: "Waterfronts",
        href: "subject/waterfronts",
        description: "Subject"
    },
    {
        text: "Wagons",
        href: "subject/wagons",
        description: "Subject"
    },
    {
        text: "Ships",
        href: "subject/ships",
        description: "Subject"
    },
    {
        text: "Universities and colleges",
        href: "subject/universities_and_colleges",
        description: "Subject"
    },
    {
        text: "Mountaineering",
        href: "subject/mountaineering",
        description: "Subject"
    },
    {
        text: "Birthday cards",
        href: "subject/birthday_cards",
        description: "Subject"
    },
    {
        text: "Resorts",
        href: "subject/resorts",
        description: "Subject"
    },
    {
        text: "Abbeys",
        href: "subject/abbeys",
        description: "Subject"
    },
    {
        text: "Arches",
        href: "subject/arches",
        description: "Subject"
    },
    {
        text: "Hills",
        href: "subject/hills",
        description: "Subject"
    },
    {
        text: "Fencing",
        href: "subject/fencing",
        description: "Subject"
    },
    {
        text: "City halls",
        href: "subject/city_halls",
        description: "Subject"
    },
    {
        text: "Rowboats",
        href: "subject/rowboats",
        description: "Subject"
    },
    {
        text: "Courtyards",
        href: "subject/courtyards",
        description: "Subject"
    },
    {
        text: "Stores, Retail",
        href: "subject/stores_retail",
        description: "Subject"
    },
    {
        text: "Still lifes",
        href: "subject/still_lifes",
        description: "Subject"
    },
    {
        text: "Waterways",
        href: "subject/waterways",
        description: "Subject"
    },
    {
        text: "Lighthouses",
        href: "subject/lighthouses",
        description: "Subject"
    },
    {
        text: "Islands",
        href: "subject/islands",
        description: "Subject"
    },
    {
        text: "Wrestling",
        href: "subject/wrestling",
        description: "Subject"
    },
    {
        text: "Plazas",
        href: "subject/plazas",
        description: "Subject"
    },
    {
        text: "Field hockey",
        href: "subject/field_hockey",
        description: "Subject"
    },
    {
        text: "Sculptures",
        href: "subject/sculptures",
        description: "Subject"
    },
    {
        text: "Fortification",
        href: "subject/fortification",
        description: "Subject"
    },
    {
        text: "Canals",
        href: "subject/canals",
        description: "Subject"
    },
    {
        text: "Religious facilities",
        href: "subject/religious_facilities",
        description: "Subject"
    },
    {
        text: "Fountains",
        href: "subject/fountains",
        description: "Subject"
    },
    {
        text: "Dirt roads",
        href: "subject/dirt_roads",
        description: "Subject"
    },
    {
        text: "Libraries",
        href: "subject/libraries",
        description: "Subject"
    },
    {
        text: "Schools",
        href: "subject/schools",
        description: "Subject"
    },
    {
        text: "New Year cards",
        href: "subject/new_year_cards",
        description: "Subject"
    },
    {
        text: "Taverns (Inns)",
        href: "subject/taverns",
        description: "Subject"
    },
    {
        text: "Employees",
        href: "subject/employees",
        description: "Subject"
    },
    {
        text: "Theaters",
        href: "subject/theaters",
        description: "Subject"
    },
    {
        text: "Shopping",
        href: "subject/shopping",
        description: "Subject"
    },
    {
        text: "Saint Patrick's Day",
        href: "subject/saint_patricks_day",
        description: "Subject"
    },
    {
        text: "Business districts",
        href: "subject/business_districts",
        description: "Subject"
    },
    {
        text: "Newberry Library",
        href: "subject/newberry_library",
        description: "Subject"
    },
    {
        text: "Hospitals",
        href: "subject/hospitals",
        description: "Subject"
    },
    {
        text: "Sports facilities",
        href: "subject/sports_facilities",
        description: "Subject"
    },
    {
        text: "Mills and mill-work",
        href: "subject/mills_and_millwork",
        description: "Subject"
    },
    {
        text: "Soldiers",
        href: "subject/soldiers",
        description: "Subject"
    },
    {
        text: "Stairways",
        href: "subject/stairways",
        description: "Subject"
    },
    {
        text: "Croquet",
        href: "subject/croquet",
        description: "Subject"
    },
    {
        text: "Shepherds",
        href: "subject/shepherds",
        description: "Subject"
    },
    {
        text: "Docks",
        href: "subject/docks",
        description: "Subject"
    },
    {
        text: "People",
        href: "subject/people",
        description: "Subject"
    },
    {
        text: "Sailors",
        href: "subject/sailors",
        description: "Subject"
    },
    {
        text: "Deserts",
        href: "subject/deserts",
        description: "Subject"
    },
    {
        text: "Stereotypes",
        href: "subject/stereotypes",
        description: "Subject"
    },
    {
        text: "Caves",
        href: "subject/caves",
        description: "Subject"
    },
    {
        text: "Railroad stations",
        href: "subject/railroad_stations",
        description: "Subject"
    },
    {
        text: "Bicycles & tricycles",
        href: "subject/bicycles__tricycles",
        description: "Subject"
    },
    {
        text: "Ferries",
        href: "subject/ferries",
        description: "Subject"
    },
    {
        text: "Indigenous peoples",
        href: "subject/indigenous_peoples",
        description: "Subject"
    },
    {
        text: "Historic buildings",
        href: "subject/historic_buildings",
        description: "Subject"
    },
    {
        text: "Railroads",
        href: "subject/railroads",
        description: "Subject"
    },
    {
        text: "Mosques",
        href: "subject/mosques",
        description: "Subject"
    },
    {
        text: "Post offices",
        href: "subject/post_offices",
        description: "Subject"
    },
    {
        text: "Street-railroads",
        href: "subject/streetrailroads",
        description: "Subject"
    },
    {
        text: "Furniture",
        href: "subject/furniture",
        description: "Subject"
    },
    {
        text: "Interior architecture",
        href: "subject/interior_architecture",
        description: "Subject"
    },
    {
        text: "Monasteries",
        href: "subject/monasteries",
        description: "Subject"
    },
    {
        text: "Authors",
        href: "subject/authors",
        description: "Subject"
    },
    {
        text: "Banks and banking",
        href: "subject/banks_and_banking",
        description: "Subject"
    },
    {
        text: "Women",
        href: "subject/women",
        description: "Subject"
    },
    {
        text: "Military facilities",
        href: "subject/military_facilities",
        description: "Subject"
    },
    {
        text: "Restaurants",
        href: "subject/restaurants",
        description: "Subject"
    },
    {
        text: "Alleys",
        href: "subject/alleys",
        description: "Subject"
    },
    {
        text: "Church buildings",
        href: "subject/church_buildings",
        description: "Subject"
    },
    {
        text: "Buildings",
        href: "subject/buildings",
        description: "Subject"
    },
    {
        text: "Bell towers",
        href: "subject/bell_towers",
        description: "Subject"
    },
    {
        text: "Educational facilities",
        href: "subject/educational_facilities",
        description: "Subject"
    },
    {
        text: "Canyons",
        href: "subject/canyons",
        description: "Subject"
    },
    {
        text: "Dance",
        href: "subject/dance",
        description: "Subject"
    },
    {
        text: "Chapels",
        href: "subject/chapels",
        description: "Subject"
    },
    {
        text: "Pagodas",
        href: "subject/pagodas",
        description: "Subject"
    },
    {
        text: "Springs",
        href: "subject/springs",
        description: "Subject"
    },
    {
        text: "Peddlers",
        href: "subject/peddlers",
        description: "Subject"
    },
    {
        text: "Historic sites",
        href: "subject/historic_sites",
        description: "Subject"
    },
    {
        text: "City and town life",
        href: "subject/city_and_town_life",
        description: "Subject"
    },
    {
        text: "Automobiles",
        href: "subject/automobiles",
        description: "Subject"
    },
    {
        text: "Places Of Worship",
        href: "subject/places_of_worship",
        description: "Subject"
    },
    {
        text: "Livestock",
        href: "subject/livestock",
        description: "Subject"
    },
    {
        text: "Prisons",
        href: "subject/prisons",
        description: "Subject"
    },
    {
        text: "Thanksgiving",
        href: "subject/thanksgiving",
        description: "Subject"
    },
    {
        text: "Halloween",
        href: "subject/halloween",
        description: "Subject"
    },
    {
        text: "Battlefields",
        href: "subject/battlefields",
        description: "Subject"
    },
    {
        text: "Glaciers",
        href: "subject/glaciers",
        description: "Subject"
    },
    {
        text: "Fjords",
        href: "subject/fjords",
        description: "Subject"
    },
    {
        text: "Angels",
        href: "subject/angels",
        description: "Subject"
    },
    {
        text: "Capitols",
        href: "subject/capitols",
        description: "Subject"
    },
    {
        text: "Forests",
        href: "subject/forests",
        description: "Subject"
    },
    {
        text: "Shrines",
        href: "subject/shrines",
        description: "Subject"
    },
    {
        text: "Buenos Aires (Argentina)",
        href: "subject/buenos_aires",
        description: "Subject"
    },
    {
        text: "Golliwogs",
        href: "subject/golliwogs",
        description: "Subject"
    },
    {
        text: "Festivals",
        href: "subject/festivals",
        description: "Subject"
    },
    {
        text: "Lahore (Pakistan)",
        href: "subject/lahore",
        description: "Subject"
    },
    {
        text: "Oceans",
        href: "subject/oceans",
        description: "Subject"
    },
    {
        text: "Singapore",
        href: "subject/singapore",
        description: "Subject"
    },
    {
        text: "Houses",
        href: "subject/houses",
        description: "Subject"
    },
    {
        text: "Peasants",
        href: "subject/peasants",
        description: "Subject"
    },
    {
        text: "Birthplaces",
        href: "subject/birthplaces",
        description: "Subject"
    },
    {
        text: "Mansions",
        href: "subject/mansions",
        description: "Subject"
    },
    {
        text: "Health resorts",
        href: "subject/health_resorts",
        description: "Subject"
    },
    {
        text: "Casinos",
        href: "subject/casinos",
        description: "Subject"
    },
    {
        text: "George Town (Pulau Pinang, Malaysia)",
        href: "subject/george_town",
        description: "Subject"
    },
    {
        text: "Rocks",
        href: "subject/rocks",
        description: "Subject"
    },
    {
        text: "Swans",
        href: "subject/swans",
        description: "Subject"
    },
    {
        text: "Flags",
        href: "subject/flags",
        description: "Subject"
    },
    {
        text: "Sunrises & sunsets",
        href: "subject/sunrises__sunsets",
        description: "Subject"
    },
    {
        text: "Equestrian statues",
        href: "subject/equestrian_statues",
        description: "Subject"
    },
    {
        text: "Students",
        href: "subject/students",
        description: "Subject"
    },
    {
        text: "Channels",
        href: "subject/channels",
        description: "Subject"
    },
    {
        text: "Animals",
        href: "subject/animals",
        description: "Subject"
    },
    {
        text: "Mining",
        href: "subject/mining",
        description: "Subject"
    },
    {
        text: "Factories",
        href: "subject/factories",
        description: "Subject"
    },
    {
        text: "Rural conditions",
        href: "subject/rural_conditions",
        description: "Subject"
    },
    {
        text: "Tourists",
        href: "subject/tourists",
        description: "Subject"
    },
    {
        text: "Agricultural laborers",
        href: "subject/agricultural_laborers",
        description: "Subject"
    },
    {
        text: "Herding",
        href: "subject/herding",
        description: "Subject"
    },
    {
        text: "Wild flowers",
        href: "subject/wild_flowers",
        description: "Subject"
    },
    {
        text: "Sports",
        href: "subject/sports",
        description: "Subject"
    },
    {
        text: "Colonnades",
        href: "subject/colonnades",
        description: "Subject"
    },
    {
        text: "Nature",
        href: "subject/nature",
        description: "Subject"
    },
    {
        text: "Black people",
        href: "subject/black_people",
        description: "Subject"
    },
    {
        text: "Cats",
        href: "subject/cats",
        description: "Subject"
    },
    {
        text: "Fairs",
        href: "subject/fairs",
        description: "Subject"
    },
    {
        text: "Domestic life",
        href: "subject/domestic_life",
        description: "Subject"
    },
    {
        text: "Fiji",
        href: "subject/fiji",
        description: "Subject"
    },
    {
        text: "Lakes & ponds",
        href: "subject/lakes__ponds",
        description: "Subject"
    },
    {
        text: "Deer",
        href: "subject/deer",
        description: "Subject"
    },
    {
        text: "Clocks and watches",
        href: "subject/clocks_and_watches",
        description: "Subject"
    },
    {
        text: "Airplanes",
        href: "subject/airplanes",
        description: "Subject"
    },
    {
        text: "Winter",
        href: "subject/winter",
        description: "Subject"
    },
    {
        text: "Walls",
        href: "subject/walls",
        description: "Subject"
    },
    {
        text: "Gibraltar",
        href: "subject/gibraltar",
        description: "Subject"
    },
    {
        text: "Estates",
        href: "subject/estates",
        description: "Subject"
    },
    {
        text: "Gdańsk (Poland)",
        href: "subject/gdansk",
        description: "Subject"
    },
    {
        text: "Holly",
        href: "subject/holly",
        description: "Subject"
    },
    {
        text: "Clothing and dress",
        href: "subject/clothing_and_dress",
        description: "Subject"
    },
    {
        text: "Sleds",
        href: "subject/sleds",
        description: "Subject"
    },
    {
        text: "Trails & paths",
        href: "subject/trails__paths",
        description: "Subject"
    },
    {
        text: "Bird's-eye views",
        href: "subject/birdseye_views",
        description: "Subject"
    },
    {
        text: "Coastlines",
        href: "subject/coastlines",
        description: "Subject"
    },
    {
        text: "Alcoholic beverages",
        href: "subject/alcoholic_beverages",
        description: "Subject"
    },
    {
        text: "Wells",
        href: "subject/wells",
        description: "Subject"
    },
    {
        text: "Windmills",
        href: "subject/windmills",
        description: "Subject"
    },
    {
        text: "Moats",
        href: "subject/moats",
        description: "Subject"
    },
    {
        text: "Fishing boats",
        href: "subject/fishing_boats",
        description: "Subject"
    },
    {
        text: "Parades",
        href: "subject/parades",
        description: "Subject"
    },
    {
        text: "Sculpture",
        href: "subject/sculpture",
        description: "Subject"
    },
    {
        text: "Synagogues",
        href: "subject/synagogues",
        description: "Subject"
    },
    {
        text: "Farmers",
        href: "subject/farmers",
        description: "Subject"
    },
    {
        text: "Agriculture",
        href: "subject/agriculture",
        description: "Subject"
    },
    {
        text: "Gondolas",
        href: "subject/gondolas",
        description: "Subject"
    },
    {
        text: "Causeways",
        href: "subject/causeways",
        description: "Subject"
    },
    {
        text: "Pools",
        href: "subject/pools",
        description: "Subject"
    },
    {
        text: "Poland",
        href: "subject/poland",
        description: "Subject"
    },
    {
        text: "Religion",
        href: "subject/religion",
        description: "Subject"
    },
    {
        text: "Lawns",
        href: "subject/lawns",
        description: "Subject"
    },
    {
        text: "Hunters",
        href: "subject/hunters",
        description: "Subject"
    },
    {
        text: "Municipal buildings",
        href: "subject/municipal_buildings",
        description: "Subject"
    },
    {
        text: "Huts",
        href: "subject/huts",
        description: "Subject"
    },
    {
        text: "Halls",
        href: "subject/halls",
        description: "Subject"
    },
    {
        text: "Quadrangles (Courtyards)",
        href: "subject/quadrangles",
        description: "Subject"
    },
    {
        text: "Air pollution",
        href: "subject/air_pollution",
        description: "Subject"
    },
    {
        text: "Streams",
        href: "subject/streams",
        description: "Subject"
    },
    {
        text: "Carriages & coaches",
        href: "subject/carriages__coaches",
        description: "Subject"
    },
    {
        text: "Carts & wagons",
        href: "subject/carts__wagons",
        description: "Subject"
    },
    {
        text: "Stained glass",
        href: "subject/stained_glass",
        description: "Subject"
    },
    {
        text: "Cabins",
        href: "subject/cabins",
        description: "Subject"
    },
    {
        text: "Geysers",
        href: "subject/geysers",
        description: "Subject"
    },
    {
        text: "Wood-engraving",
        href: "subject/woodengraving",
        description: "Subject"
    },
    {
        text: "Families",
        href: "subject/families",
        description: "Subject"
    },
    {
        text: "Chile",
        href: "subject/chile",
        description: "Subject"
    },
    {
        text: "Musicians",
        href: "subject/musicians",
        description: "Subject"
    },
    {
        text: "Chickens",
        href: "subject/chickens",
        description: "Subject"
    },
    {
        text: "Courthouses",
        href: "subject/courthouses",
        description: "Subject"
    },
    {
        text: "Thatched roofs",
        href: "subject/thatched_roofs",
        description: "Subject"
    },
    {
        text: "Czech Republic",
        href: "subject/czech_republic",
        description: "Subject"
    },
    {
        text: "Seas",
        href: "subject/seas",
        description: "Subject"
    },
    {
        text: "Clubs",
        href: "subject/clubs",
        description: "Subject"
    },
    {
        text: "Cities & towns",
        href: "subject/cities__towns",
        description: "Subject"
    },
    {
        text: "Walkways",
        href: "subject/walkways",
        description: "Subject"
    },
    {
        text: "Weaving",
        href: "subject/weaving",
        description: "Subject"
    },
    {
        text: "Obelisks",
        href: "subject/obelisks",
        description: "Subject"
    },
    {
        text: "Spring",
        href: "subject/spring",
        description: "Subject"
    },
    {
        text: "Reservoirs",
        href: "subject/reservoirs",
        description: "Subject"
    },
    {
        text: "Basilicas",
        href: "subject/basilicas",
        description: "Subject"
    },
    {
        text: "Roses",
        href: "subject/roses",
        description: "Subject"
    },
    {
        text: "Navy-yards and naval stations",
        href: "subject/navyyards_and_naval_stations",
        description: "Subject"
    },
    {
        text: "Wharves",
        href: "subject/wharves",
        description: "Subject"
    },
    {
        text: "Weavers",
        href: "subject/weavers",
        description: "Subject"
    },
    {
        text: "Prayer",
        href: "subject/prayer",
        description: "Subject"
    },
    {
        text: "Greenhouses",
        href: "subject/greenhouses",
        description: "Subject"
    },
    {
        text: "Industry",
        href: "subject/industry",
        description: "Subject"
    },
    {
        text: "Wetlands",
        href: "subject/wetlands",
        description: "Subject"
    },
    {
        text: "Forge shops",
        href: "subject/forge_shops",
        description: "Subject"
    },
    {
        text: "Bathhouses",
        href: "subject/bathhouses",
        description: "Subject"
    },
    {
        text: "Market crosses",
        href: "subject/market_crosses",
        description: "Subject"
    },
    {
        text: "Weddings",
        href: "subject/weddings",
        description: "Subject"
    },
    {
        text: "International travel",
        href: "subject/international_travel",
        description: "Subject"
    },
    {
        text: "Elephants",
        href: "subject/elephants",
        description: "Subject"
    },
    {
        text: "Air travel",
        href: "subject/air_travel",
        description: "Subject"
    },
    {
        text: "Laboratories",
        href: "subject/laboratories",
        description: "Subject"
    },
    {
        text: "Mothers",
        href: "subject/mothers",
        description: "Subject"
    },
    {
        text: "Facades",
        href: "subject/facades",
        description: "Subject"
    },
    {
        text: "Railroad travel",
        href: "subject/railroad_travel",
        description: "Subject"
    },
    {
        text: "Entertainers",
        href: "subject/entertainers",
        description: "Subject"
    },
    {
        text: "Altars",
        href: "subject/altars",
        description: "Subject"
    },
    {
        text: "Central business districts",
        href: "subject/central_business_districts",
        description: "Subject"
    },
    {
        text: "Quarries and quarrying",
        href: "subject/quarries_and_quarrying",
        description: "Subject"
    },
    {
        text: "Moors",
        href: "subject/moors",
        description: "Subject"
    },
    {
        text: "Naves",
        href: "subject/naves",
        description: "Subject"
    },
    {
        text: "Ottawa (Ont.)",
        href: "subject/ottawa",
        description: "Subject"
    },
    {
        text: "Clubhouses",
        href: "subject/clubhouses",
        description: "Subject"
    },
    {
        text: "Trucks",
        href: "subject/trucks",
        description: "Subject"
    },
    {
        text: "Barracks",
        href: "subject/barracks",
        description: "Subject"
    },
    {
        text: "Charity",
        href: "subject/charity",
        description: "Subject"
    },
    {
        text: "Leisure",
        href: "subject/leisure",
        description: "Subject"
    },
    {
        text: "Castles & palaces",
        href: "subject/castles__palaces",
        description: "Subject"
    },
    {
        text: "Rock formations",
        href: "subject/rock_formations",
        description: "Subject"
    },
    {
        text: "Terraces",
        href: "subject/terraces",
        description: "Subject"
    },
    {
        text: "Graves",
        href: "subject/graves",
        description: "Subject"
    },
    {
        text: "Telegraph offices",
        href: "subject/telegraph_offices",
        description: "Subject"
    },
    {
        text: "Plowing",
        href: "subject/plowing",
        description: "Subject"
    },
    {
        text: "Legislative bodies",
        href: "subject/legislative_bodies",
        description: "Subject"
    },
    {
        text: "Motorcycles",
        href: "subject/motorcycles",
        description: "Subject"
    },
    {
        text: "Hay",
        href: "subject/hay",
        description: "Subject"
    },
    {
        text: "Saint Lucia",
        href: "subject/saint_lucia",
        description: "Subject"
    },
    {
        text: "Rowing",
        href: "subject/rowing",
        description: "Subject"
    },
    {
        text: "Airships",
        href: "subject/airships",
        description: "Subject"
    },
    {
        text: "Boulders",
        href: "subject/boulders",
        description: "Subject"
    },
    {
        text: "Walking",
        href: "subject/walking",
        description: "Subject"
    },
    {
        text: "Workers",
        href: "subject/workers",
        description: "Subject"
    },
    {
        text: "Boathouses",
        href: "subject/boathouses",
        description: "Subject"
    },
    {
        text: "Valentine's Day",
        href: "subject/valentines_day",
        description: "Subject"
    },
    {
        text: "Urban",
        href: "subject/urban",
        description: "Subject"
    },
    {
        text: "Bodies of water",
        href: "subject/bodies_of_water",
        description: "Subject"
    },
    {
        text: "National",
        href: "subject/national",
        description: "Subject"
    },
    {
        text: "Church",
        href: "subject/church",
        description: "Subject"
    },
    {
        text: "Waves",
        href: "subject/waves",
        description: "Subject"
    },
    {
        text: "Regattas",
        href: "subject/regattas",
        description: "Subject"
    },
    {
        text: "Teachers",
        href: "subject/teachers",
        description: "Subject"
    },
    {
        text: "Farms",
        href: "subject/farms",
        description: "Subject"
    },
    {
        text: "Merchants",
        href: "subject/merchants",
        description: "Subject"
    },
    {
        text: "Madonnas",
        href: "subject/madonnas",
        description: "Subject"
    },
    {
        text: "Tiger",
        href: "subject/tiger",
        description: "Subject"
    },
    {
        text: "Retail",
        href: "subject/retail",
        description: "Subject"
    },
    {
        text: "Pulau Pinang (Malaysia : State)",
        href: "subject/pulau_pinang",
        description: "Subject"
    },
    {
        text: "Christianity",
        href: "subject/christianity",
        description: "Subject"
    },
    {
        text: "Gulls",
        href: "subject/gulls",
        description: "Subject"
    },
    {
        text: "Cottages",
        href: "subject/cottages",
        description: "Subject"
    },
    {
        text: "Split (Croatia)",
        href: "subject/split",
        description: "Subject"
    },
    {
        text: "Gliders (Aeronautics)",
        href: "subject/gliders",
        description: "Subject"
    },
    {
        text: "Sleighs",
        href: "subject/sleighs",
        description: "Subject"
    },
    {
        text: "Organ (Musical instrument)",
        href: "subject/organ",
        description: "Subject"
    },
    {
        text: "Firearms",
        href: "subject/firearms",
        description: "Subject"
    },
    {
        text: "Cliff-dwellings",
        href: "subject/cliffdwellings",
        description: "Subject"
    },
    {
        text: "Postcards",
        href: "subject/postcards",
        description: "Subject"
    },
    {
        text: "Thanksgiving Day",
        href: "subject/thanksgiving_day",
        description: "Subject"
    },
    {
        text: "Boys",
        href: "subject/boys",
        description: "Subject"
    },
    {
        text: "Melbourne (Vic.)",
        href: "subject/melbourne",
        description: "Subject"
    },
    {
        text: "Street railroads",
        href: "subject/street_railroads",
        description: "Subject"
    },
    {
        text: "Puns (Visual works)",
        href: "subject/puns",
        description: "Subject"
    },
    {
        text: "Ethnic groups",
        href: "subject/ethnic_groups",
        description: "Subject"
    },
    {
        text: "Occupations",
        href: "subject/occupations",
        description: "Subject"
    },
    {
        text: "Transportation",
        href: "subject/transportation",
        description: "Subject"
    },
    {
        text: "Puente del Inca (Argentina)",
        href: "subject/puente_del_inca",
        description: "Subject"
    },
    {
        text: "Pakistan",
        href: "subject/pakistan",
        description: "Subject"
    },
    {
        text: "Rochor River (Singapore)",
        href: "subject/rochor_river",
        description: "Subject"
    },
    {
        text: "Prohibition",
        href: "subject/prohibition",
        description: "Subject"
    },
    {
        text: "Mount Sterling (Ky.)",
        href: "subject/mount_sterling",
        description: "Subject"
    },
    {
        text: "Paris (France)",
        href: "subject/paris",
        description: "Subject"
    },
    {
        text: "Poor",
        href: "subject/poor",
        description: "Subject"
    },
    {
        text: "Sopot (Poland)",
        href: "subject/sopot",
        description: "Subject"
    },
    {
        text: "Montréal (Québec)",
        href: "subject/montreal",
        description: "Subject"
    },
    {
        text: "Southwold (England)",
        href: "subject/southwold",
        description: "Subject"
    },
    {
        text: "Bays (Bodies of water)",
        href: "subject/bays",
        description: "Subject"
    },
    {
        text: "Sleds & sleighs",
        href: "subject/sleds__sleighs",
        description: "Subject"
    },
    {
        text: "Water-wheels",
        href: "subject/waterwheels",
        description: "Subject"
    },
    {
        text: "Channel Islands--Jersey",
        href: "subject/channel_islandsjersey",
        description: "Subject"
    },
    {
        text: "Conway",
        href: "subject/conway",
        description: "Subject"
    },
    {
        text: "Winchester (England)",
        href: "subject/winchester",
        description: "Subject"
    },
    {
        text: "Clovelly (England)",
        href: "subject/clovelly",
        description: "Subject"
    },
    {
        text: "Doors",
        href: "subject/doors",
        description: "Subject"
    },
    {
        text: "Stores & shops",
        href: "subject/stores__shops",
        description: "Subject"
    },
    {
        text: "Victoria (Hong Kong)",
        href: "subject/victoria",
        description: "Subject"
    },
    {
        text: "Filey (England)",
        href: "subject/filey",
        description: "Subject"
    },
    {
        text: "Van Winkle",
        href: "subject/van_winkle",
        description: "Subject"
    },
    {
        text: "Bars (Drinking establishments)",
        href: "subject/bars",
        description: "Subject"
    },
    {
        text: "Veterans' hospitals",
        href: "subject/veterans_hospitals",
        description: "Subject"
    },
    {
        text: "Rip (Fictitious character)",
        href: "subject/rip",
        description: "Subject"
    },
    {
        text: "Cathedrals",
        href: "subject/cathedrals",
        description: "Subject"
    },
    {
        text: "Mines and mineral resources",
        href: "subject/mines_and_mineral_resources",
        description: "Subject"
    },
    {
        text: "Government facilities",
        href: "subject/government_facilities",
        description: "Subject"
    },
    {
        text: "Vanua Levu (Fiji)",
        href: "subject/vanua_levu",
        description: "Subject"
    },
    {
        text: "Piers & wharves",
        href: "subject/piers__wharves",
        description: "Subject"
    },
    {
        text: "Advertisements",
        href: "subject/advertisements",
        description: "Subject"
    },
    {
        text: "Jersey--Faldouet",
        href: "subject/jerseyfaldouet",
        description: "Subject"
    },
    {
        text: "Rothesay",
        href: "subject/rothesay",
        description: "Subject"
    },
    {
        text: "Buses",
        href: "subject/buses",
        description: "Subject"
    },
    {
        text: "Kent (England)",
        href: "subject/kent",
        description: "Subject"
    },
    {
        text: "Ethnic stereotypes",
        href: "subject/ethnic_stereotypes",
        description: "Subject"
    },
    {
        text: "Ceylon",
        href: "subject/ceylon",
        description: "Subject"
    },
    {
        text: "The Alps",
        href: "subject/the_alps",
        description: "Subject"
    },
    {
        text: "Jersey",
        href: "subject/jersey",
        description: "Subject"
    },
    {
        text: "Szczecin (Poland)",
        href: "subject/szczecin",
        description: "Subject"
    },
    {
        text: "Tanjong Pagar (Singapore)",
        href: "subject/tanjong_pagar",
        description: "Subject"
    },
    {
        text: "Guernsey",
        href: "subject/guernsey",
        description: "Subject"
    },
    {
        text: "Siberia",
        href: "subject/siberia",
        description: "Subject"
    },
    {
        text: "New Year",
        href: "subject/new_year",
        description: "Subject"
    },
    {
        text: "Siberia--Lake Baikal",
        href: "subject/siberialake_baikal",
        description: "Subject"
    },
    {
        text: "Monuments & memorials",
        href: "subject/monuments__memorials",
        description: "Subject"
    },
    {
        text: "Hyderabad (Pakistan)",
        href: "subject/hyderabad",
        description: "Subject"
    },
    {
        text: "Roosters",
        href: "subject/roosters",
        description: "Subject"
    },
    {
        text: "Sphinxes",
        href: "subject/sphinxes",
        description: "Subject"
    },
    {
        text: "Vases",
        href: "subject/vases",
        description: "Subject"
    },
    {
        text: "Rectories",
        href: "subject/rectories",
        description: "Subject"
    },
    {
        text: "Rapids",
        href: "subject/rapids",
        description: "Subject"
    },
    {
        text: "Locks (hydraulic engineering)",
        href: "subject/locks",
        description: "Subject"
    },
    {
        text: "Boardwalks",
        href: "subject/boardwalks",
        description: "Subject"
    },
    {
        text: "Pulpits",
        href: "subject/pulpits",
        description: "Subject"
    },
    {
        text: "Postal service",
        href: "subject/postal_service",
        description: "Subject"
    },
    {
        text: "Telegraph",
        href: "subject/telegraph",
        description: "Subject"
    },
    {
        text: "Lifeboats",
        href: "subject/lifeboats",
        description: "Subject"
    },
    {
        text: "Jetties",
        href: "subject/jetties",
        description: "Subject"
    },
    {
        text: "Polar bears",
        href: "subject/polar_bears",
        description: "Subject"
    },
    {
        text: "Auroras",
        href: "subject/auroras",
        description: "Subject"
    },
    {
        text: "Oxen",
        href: "subject/oxen",
        description: "Subject"
    },
    {
        text: "Social life",
        href: "subject/social_life",
        description: "Subject"
    },
    {
        text: "Houseboats",
        href: "subject/houseboats",
        description: "Subject"
    },
    {
        text: "Prayer Wheels",
        href: "subject/prayer_wheels",
        description: "Subject"
    },
    {
        text: "Salutations",
        href: "subject/salutations",
        description: "Subject"
    },
    {
        text: "Pathway",
        href: "subject/pathway",
        description: "Subject"
    },
    {
        text: "Stepping stones",
        href: "subject/stepping_stones",
        description: "Subject"
    },
    {
        text: "Yachts",
        href: "subject/yachts",
        description: "Subject"
    },
    {
        text: "Hunting lodges",
        href: "subject/hunting_lodges",
        description: "Subject"
    },
    {
        text: "Bungalows",
        href: "subject/bungalows",
        description: "Subject"
    },
    {
        text: "Donkeys",
        href: "subject/donkeys",
        description: "Subject"
    },
    {
        text: "Poultry",
        href: "subject/poultry",
        description: "Subject"
    },
    {
        text: "Minarets",
        href: "subject/minarets",
        description: "Subject"
    },
    {
        text: "City walls",
        href: "subject/city_walls",
        description: "Subject"
    },
    {
        text: "Vehicles",
        href: "subject/vehicles",
        description: "Subject"
    },
    {
        text: "Chairs",
        href: "subject/chairs",
        description: "Subject"
    },
    {
        text: "Straits",
        href: "subject/straits",
        description: "Subject"
    },
    {
        text: "Bears",
        href: "subject/bears",
        description: "Subject"
    },
    {
        text: "Snakes",
        href: "subject/snakes",
        description: "Subject"
    },
    {
        text: "Cargo ships",
        href: "subject/cargo_ships",
        description: "Subject"
    },
    {
        text: "Stone buildings",
        href: "subject/stone_buildings",
        description: "Subject"
    },
    {
        text: "Paintings",
        href: "subject/paintings",
        description: "Subject"
    },
    {
        text: "Picnics",
        href: "subject/picnics",
        description: "Subject"
    },
    {
        text: "Ferns",
        href: "subject/ferns",
        description: "Subject"
    },
    {
        text: "Birdbaths",
        href: "subject/birdbaths",
        description: "Subject"
    },
    {
        text: "Fakirs",
        href: "subject/fakirs",
        description: "Subject"
    },
    {
        text: "Trinidad and Tobago",
        href: "subject/trinidad_and_tobago",
        description: "Subject"
    },
    {
        text: "Castle Cornet (Saint Peter Port",
        href: "subject/castle_cornet_saint_peter_port",
        description: "Subject"
    },
    {
        text: "Water carriers",
        href: "subject/water_carriers",
        description: "Subject"
    },
    {
        text: "Delaware",
        href: "subject/delaware",
        description: "Subject"
    },
    {
        text: "Iowa",
        href: "subject/iowa",
        description: "Subject"
    },
    {
        text: "Southern States",
        href: "subject/southern_states",
        description: "Subject"
    },
    {
        text: "Rye",
        href: "subject/rye",
        description: "Subject"
    },
    {
        text: "Toys",
        href: "subject/toys",
        description: "Subject"
    },
    {
        text: "Baking",
        href: "subject/baking",
        description: "Subject"
    },
    {
        text: "Infants",
        href: "subject/infants",
        description: "Subject"
    },
    {
        text: "Travelers",
        href: "subject/travelers",
        description: "Subject"
    },
    {
        text: "Rabbits",
        href: "subject/rabbits",
        description: "Subject"
    },
    {
        text: "Exhibition buildings",
        href: "subject/exhibition_buildings",
        description: "Subject"
    },
    {
        text: "Labels",
        href: "subject/labels",
        description: "Subject"
    },
    {
        text: "Advertising",
        href: "subject/advertising",
        description: "Subject"
    },
    {
        text: "Marriage",
        href: "subject/marriage",
        description: "Subject"
    },
    {
        text: "Travel",
        href: "subject/travel",
        description: "Subject"
    },
    {
        text: "Architecture",
        href: "subject/architecture",
        description: "Subject"
    },
    {
        text: "Music",
        href: "subject/music",
        description: "Subject"
    },
    {
        text: "The",
        href: "subject/the",
        description: "Subject"
    },
    {
        text: "Water",
        href: "subject/water",
        description: "Subject"
    },
    {
        text: "Love-letters",
        href: "subject/loveletters",
        description: "Subject"
    },
    {
        text: "Girls",
        href: "subject/girls",
        description: "Subject"
    },
    {
        text: "Food processing",
        href: "subject/food_processing",
        description: "Subject"
    },
    {
        text: "Tunnels",
        href: "subject/tunnels",
        description: "Subject"
    },
    {
        text: "Races",
        href: "subject/races",
        description: "Subject"
    },
    {
        text: "Shipwrecks",
        href: "subject/shipwrecks",
        description: "Subject"
    },
    {
        text: "Scenery",
        href: "subject/scenery",
        description: "Subject"
    },
    {
        text: "Events",
        href: "subject/events",
        description: "Subject"
    },
    {
        text: "Societies",
        href: "subject/societies",
        description: "Subject"
    },
    {
        text: "Education",
        href: "subject/education",
        description: "Subject"
    },
    {
        text: "Marine art",
        href: "subject/marine_art",
        description: "Subject"
    },
    {
        text: "House furnishings",
        href: "subject/house_furnishings",
        description: "Subject"
    },
    {
        text: "Housing",
        href: "subject/housing",
        description: "Subject"
    },
    {
        text: "Ghats (Architecture)",
        href: "subject/ghats",
        description: "Subject"
    },
    {
        text: "Thailand",
        href: "subject/thailand",
        description: "Subject"
    },
    {
        text: "Penguins",
        href: "subject/penguins",
        description: "Subject"
    },
    {
        text: "Chinese",
        href: "subject/chinese",
        description: "Subject"
    },
    {
        text: "Singing",
        href: "subject/singing",
        description: "Subject"
    },
    {
        text: "Night",
        href: "subject/night",
        description: "Subject"
    },
    {
        text: "Rickshaws",
        href: "subject/rickshaws",
        description: "Subject"
    },
    {
        text: "Coconuts",
        href: "subject/coconuts",
        description: "Subject"
    },
    {
        text: "Lifts",
        href: "subject/lifts",
        description: "Subject"
    },
    {
        text: "Ice",
        href: "subject/ice",
        description: "Subject"
    },
    {
        text: "Clover",
        href: "subject/clover",
        description: "Subject"
    },
    {
        text: "Barns",
        href: "subject/barns",
        description: "Subject"
    },
    {
        text: "Convents",
        href: "subject/convents",
        description: "Subject"
    },
    {
        text: "Caravans",
        href: "subject/caravans",
        description: "Subject"
    },
    {
        text: "Rugby",
        href: "subject/rugby",
        description: "Subject"
    },
    {
        text: "Knitting",
        href: "subject/knitting",
        description: "Subject"
    },
    {
        text: "Beadwork",
        href: "subject/beadwork",
        description: "Subject"
    },
    {
        text: "Joking",
        href: "subject/joking",
        description: "Subject"
    },
    {
        text: "Rainbows",
        href: "subject/rainbows",
        description: "Subject"
    },
    {
        text: "Threshing",
        href: "subject/threshing",
        description: "Subject"
    },
    {
        text: "Cairns",
        href: "subject/cairns",
        description: "Subject"
    },
    {
        text: "Bells",
        href: "subject/bells",
        description: "Subject"
    },
    {
        text: "Cloisters",
        href: "subject/cloisters",
        description: "Subject"
    },
    {
        text: "Water towers",
        href: "subject/water_towers",
        description: "Subject"
    },
    {
        text: "Boundary walls",
        href: "subject/boundary_walls",
        description: "Subject"
    },
    {
        text: "Mules",
        href: "subject/mules",
        description: "Subject"
    },
    {
        text: "May poles",
        href: "subject/may_poles",
        description: "Subject"
    },
    {
        text: "Ducks",
        href: "subject/ducks",
        description: "Subject"
    },
    {
        text: "Skiing",
        href: "subject/skiing",
        description: "Subject"
    },
    {
        text: "Tulips",
        href: "subject/tulips",
        description: "Subject"
    },
    {
        text: "Mistletoe",
        href: "subject/mistletoe",
        description: "Subject"
    },
    {
        text: "Sea walls",
        href: "subject/sea_walls",
        description: "Subject"
    },
    {
        text: "Rose windows",
        href: "subject/rose_windows",
        description: "Subject"
    },
    {
        text: "Pigeons",
        href: "subject/pigeons",
        description: "Subject"
    },
    {
        text: "Argentina",
        href: "subject/argentina",
        description: "Subject"
    },
    {
        text: "Monks",
        href: "subject/monks",
        description: "Subject"
    },
    {
        text: "Court yards",
        href: "subject/court_yards",
        description: "Subject"
    },
    {
        text: "Logs",
        href: "subject/logs",
        description: "Subject"
    },
    {
        text: "Goats",
        href: "subject/goats",
        description: "Subject"
    },
    {
        text: "Veterans",
        href: "subject/veterans",
        description: "Subject"
    },
    {
        text: "Plants",
        href: "subject/plants",
        description: "Subject"
    },
    {
        text: "Public buildings",
        href: "subject/public_buildings",
        description: "Subject"
    },
    {
        text: "Terraces (Land use)",
        href: "subject/terraces",
        description: "Subject"
    },
    {
        text: "Legends",
        href: "subject/legends",
        description: "Subject"
    },
    {
        text: "Sailing ships",
        href: "subject/sailing_ships",
        description: "Subject"
    },
    {
        text: "Biblical events",
        href: "subject/biblical_events",
        description: "Subject"
    },
    {
        text: "Cherry orchards",
        href: "subject/cherry_orchards",
        description: "Subject"
    },
    {
        text: "Canal boats",
        href: "subject/canal_boats",
        description: "Subject"
    },
    {
        text: "Financial facilities",
        href: "subject/financial_facilities",
        description: "Subject"
    },
    {
        text: "Warships",
        href: "subject/warships",
        description: "Subject"
    },
    {
        text: "Concert halls",
        href: "subject/concert_halls",
        description: "Subject"
    },
    {
        text: "Alps",
        href: "subject/alps",
        description: "Subject"
    },
    {
        text: "Baskets",
        href: "subject/baskets",
        description: "Subject"
    },
    {
        text: "Magicians",
        href: "subject/magicians",
        description: "Subject"
    },
    {
        text: "Rugs",
        href: "subject/rugs",
        description: "Subject"
    },
    {
        text: "Lilies",
        href: "subject/lilies",
        description: "Subject"
    },
    {
        text: "Gaols",
        href: "subject/gaols",
        description: "Subject"
    },
    {
        text: "Water lilies",
        href: "subject/water_lilies",
        description: "Subject"
    },
    {
        text: "Ice floes",
        href: "subject/ice_floes",
        description: "Subject"
    },
    {
        text: "Roofs",
        href: "subject/roofs",
        description: "Subject"
    },
    {
        text: "Tea",
        href: "subject/tea",
        description: "Subject"
    },
    {
        text: "Spinning",
        href: "subject/spinning",
        description: "Subject"
    },
    {
        text: "Seaports",
        href: "subject/seaports",
        description: "Subject"
    },
    {
        text: "Locks",
        href: "subject/locks",
        description: "Subject"
    },
    {
        text: "Brewery",
        href: "subject/brewery",
        description: "Subject"
    },
    {
        text: "Student organizations",
        href: "subject/student_organizations",
        description: "Subject"
    },
    {
        text: "Esplanades",
        href: "subject/esplanades",
        description: "Subject"
    },
    {
        text: "Cafes",
        href: "subject/cafes",
        description: "Subject"
    },
    {
        text: "Jewelry",
        href: "subject/jewelry",
        description: "Subject"
    },
    {
        text: "Stock exchanges",
        href: "subject/stock_exchanges",
        description: "Subject"
    },
    {
        text: "Canoes",
        href: "subject/canoes",
        description: "Subject"
    },
    {
        text: "Miners",
        href: "subject/miners",
        description: "Subject"
    },
    {
        text: "Cable railroads",
        href: "subject/cable_railroads",
        description: "Subject"
    },
    {
        text: "Lions",
        href: "subject/lions",
        description: "Subject"
    },
    {
        text: "Giraffes",
        href: "subject/giraffes",
        description: "Subject"
    },
    {
        text: "Windows",
        href: "subject/windows",
        description: "Subject"
    },
    {
        text: "Cradles",
        href: "subject/cradles",
        description: "Subject"
    },
    {
        text: "Monorail railroads",
        href: "subject/monorail_railroads",
        description: "Subject"
    },
    {
        text: "Pedestrian bridges",
        href: "subject/pedestrian_bridges",
        description: "Subject"
    },
    {
        text: "Birches",
        href: "subject/birches",
        description: "Subject"
    },
    {
        text: "Puddles",
        href: "subject/puddles",
        description: "Subject"
    },
    {
        text: "Ice skating",
        href: "subject/ice_skating",
        description: "Subject"
    },
    {
        text: "Crosses",
        href: "subject/crosses",
        description: "Subject"
    },
    {
        text: "Tropical forests",
        href: "subject/tropical_forests",
        description: "Subject"
    },
    {
        text: "Graveyards",
        href: "subject/graveyards",
        description: "Subject"
    },
    {
        text: "Guards",
        href: "subject/guards",
        description: "Subject"
    },
    {
        text: "Pigs",
        href: "subject/pigs",
        description: "Subject"
    },
]

export const place = [{
        text: "England--Surrey",
        href: "place/englandsurrey",
        description: "Place"
    },
    {
        text: "England",
        href: "place/england",
        description: "Place"
    },
    {
        text: "England--London",
        href: "place/englandlondon",
        description: "Place"
    },
    {
        text: "California--San Francisco",
        href: "place/californiasan_francisco",
        description: "Place"
    },
    {
        text: "Japan",
        href: "place/japan",
        description: "Place"
    },
    {
        text: "Scotland",
        href: "place/scotland",
        description: "Place"
    },
    {
        text: "France--Paris",
        href: "place/franceparis",
        description: "Place"
    },
    {
        text: "England--Devon",
        href: "place/englanddevon",
        description: "Place"
    },
    {
        text: "Scotland--Edinburgh",
        href: "place/scotlandedinburgh",
        description: "Place"
    },
    {
        text: "England--Isle of Wight",
        href: "place/englandisle_of_wight",
        description: "Place"
    },
    {
        text: "England--Cornwall (County)",
        href: "place/englandcornwall",
        description: "Place"
    },
    {
        text: "Georgia--Atlanta",
        href: "place/georgiaatlanta",
        description: "Place"
    },
    {
        text: "England--Whitby",
        href: "place/englandwhitby",
        description: "Place"
    },
    {
        text: "Illinois--Chicago",
        href: "place/illinoischicago",
        description: "Place"
    },
    {
        text: "France--Nice",
        href: "place/francenice",
        description: "Place"
    },
    {
        text: "England--Warwickshire",
        href: "place/englandwarwickshire",
        description: "Place"
    },
    {
        text: "England--Scarborough",
        href: "place/englandscarborough",
        description: "Place"
    },
    {
        text: "Wales",
        href: "place/wales",
        description: "Place"
    },
    {
        text: "Great Britain Miscellaneous Island Dependencies--Isle of Man",
        href: "place/great_britain_miscellaneous_island_dependenciesisle_of_man",
        description: "Place"
    },
    {
        text: "England--York",
        href: "place/englandyork",
        description: "Place"
    },
    {
        text: "England--Oxford",
        href: "place/englandoxford",
        description: "Place"
    },
    {
        text: "Monaco--Monte-Carlo",
        href: "place/monacomontecarlo",
        description: "Place"
    },
    {
        text: "England--Stratford-upon-Avon",
        href: "place/englandstratforduponavon",
        description: "Place"
    },
    {
        text: "Italy",
        href: "place/italy",
        description: "Place"
    },
    {
        text: "England--Warwick",
        href: "place/englandwarwick",
        description: "Place"
    },
    {
        text: "Italy--Venice",
        href: "place/italyvenice",
        description: "Place"
    },
    {
        text: "England--Yorkshire",
        href: "place/englandyorkshire",
        description: "Place"
    },
    {
        text: "England--Norfolk",
        href: "place/englandnorfolk",
        description: "Place"
    },
    {
        text: "France",
        href: "place/france",
        description: "Place"
    },
    {
        text: "England--Derbyshire",
        href: "place/englandderbyshire",
        description: "Place"
    },
    {
        text: "Que?bec--Que?bec",
        href: "place/quebecquebec",
        description: "Place"
    },
    {
        text: "England--Torquay",
        href: "place/englandtorquay",
        description: "Place"
    },
    {
        text: "France--Menton",
        href: "place/francementon",
        description: "Place"
    },
    {
        text: "Scotland--Trossachs",
        href: "place/scotlandtrossachs",
        description: "Place"
    },
    {
        text: "England--Hastings",
        href: "place/englandhastings",
        description: "Place"
    },
    {
        text: "France--Rouen",
        href: "place/francerouen",
        description: "Place"
    },
    {
        text: "England--Southport",
        href: "place/englandsouthport",
        description: "Place"
    },
    {
        text: "England--Eastbourne (East Sussex)",
        href: "place/englandeastbourne",
        description: "Place"
    },
    {
        text: "England--Kent",
        href: "place/englandkent",
        description: "Place"
    },
    {
        text: "India",
        href: "place/india",
        description: "Place"
    },
    {
        text: "France--Cannes",
        href: "place/francecannes",
        description: "Place"
    },
    {
        text: "Australia",
        href: "place/australia",
        description: "Place"
    },
    {
        text: "England--Sussex",
        href: "place/englandsussex",
        description: "Place"
    },
    {
        text: "England--Liverpool",
        href: "place/englandliverpool",
        description: "Place"
    },
    {
        text: "England--Dorset",
        href: "place/englanddorset",
        description: "Place"
    },
    {
        text: "Italy--Merano",
        href: "place/italymerano",
        description: "Place"
    },
    {
        text: "France--Marseille",
        href: "place/francemarseille",
        description: "Place"
    },
    {
        text: "England--Berkshire",
        href: "place/englandberkshire",
        description: "Place"
    },
    {
        text: "England--Canterbury",
        href: "place/englandcanterbury",
        description: "Place"
    },
    {
        text: "Scotland--Perthshire",
        href: "place/scotlandperthshire",
        description: "Place"
    },
    {
        text: "Scotland--Inverness",
        href: "place/scotlandinverness",
        description: "Place"
    },
    {
        text: "England--Harrogate",
        href: "place/englandharrogate",
        description: "Place"
    },
    {
        text: "England--Brighton",
        href: "place/englandbrighton",
        description: "Place"
    },
    {
        text: "England--Blackpool",
        href: "place/englandblackpool",
        description: "Place"
    },
    {
        text: "France--Aix-les-Bains",
        href: "place/franceaixlesbains",
        description: "Place"
    },
    {
        text: "England--Gloucester",
        href: "place/englandgloucester",
        description: "Place"
    },
    {
        text: "Scotland--Glasgow",
        href: "place/scotlandglasgow",
        description: "Place"
    },
    {
        text: "England--Lincoln",
        href: "place/englandlincoln",
        description: "Place"
    },
    {
        text: "Scotland--Stirling (Stirling)",
        href: "place/scotlandstirling",
        description: "Place"
    },
    {
        text: "England--Clovelly",
        href: "place/englandclovelly",
        description: "Place"
    },
    {
        text: "Ontario--Ottawa",
        href: "place/ontarioottawa",
        description: "Place"
    },
    {
        text: "Wales--Betws-y-Coed",
        href: "place/walesbetwsycoed",
        description: "Place"
    },
    {
        text: "England--Worcester",
        href: "place/englandworcester",
        description: "Place"
    },
    {
        text: "United States",
        href: "place/united_states",
        description: "Place"
    },
    {
        text: "England--Bristol",
        href: "place/englandbristol",
        description: "Place"
    },
    {
        text: "England--Essex",
        href: "place/englandessex",
        description: "Place"
    },
    {
        text: "England--Rochester (Kent)",
        href: "place/englandrochester",
        description: "Place"
    },
    {
        text: "England--Chester",
        href: "place/englandchester",
        description: "Place"
    },
    {
        text: "France--Dieppe",
        href: "place/francedieppe",
        description: "Place"
    },
    {
        text: "England--Sheffield",
        href: "place/englandsheffield",
        description: "Place"
    },
    {
        text: "Scotland--Aberdeen",
        href: "place/scotlandaberdeen",
        description: "Place"
    },
    {
        text: "Great Britain Miscellaneous Island Dependencies--Guernsey",
        href: "place/great_britain_miscellaneous_island_dependenciesguernsey",
        description: "Place"
    },
    {
        text: "England--Southend-on-Sea",
        href: "place/englandsouthendonsea",
        description: "Place"
    },
    {
        text: "France--Lyon",
        href: "place/francelyon",
        description: "Place"
    },
    {
        text: "England--Bath",
        href: "place/englandbath",
        description: "Place"
    },
    {
        text: "Italy--Florence",
        href: "place/italyflorence",
        description: "Place"
    },
    {
        text: "Great Britain Miscellaneous Island Dependencies--Jersey",
        href: "place/great_britain_miscellaneous_island_dependenciesjersey",
        description: "Place"
    },
    {
        text: "England--Norwich",
        href: "place/englandnorwich",
        description: "Place"
    },
    {
        text: "New York (State)--New York",
        href: "place/new_york_statenew_york",
        description: "Place"
    },
    {
        text: "England--Exeter",
        href: "place/englandexeter",
        description: "Place"
    },
    {
        text: "Scotland--Argyllshire",
        href: "place/scotlandargyllshire",
        description: "Place"
    },
    {
        text: "England--Shrewsbury",
        href: "place/englandshrewsbury",
        description: "Place"
    },
    {
        text: "Scotland--Strathpeffer",
        href: "place/scotlandstrathpeffer",
        description: "Place"
    },
    {
        text: "Outer space",
        href: "place/outer_space",
        description: "Place"
    },
    {
        text: "Scotland--Oban",
        href: "place/scotlandoban",
        description: "Place"
    },
    {
        text: "Italy--Verona",
        href: "place/italyverona",
        description: "Place"
    },
    {
        text: "England--Derwent Water",
        href: "place/englandderwent_water",
        description: "Place"
    },
    {
        text: "England--Buxton (Derbyshire)",
        href: "place/englandbuxton",
        description: "Place"
    },
    {
        text: "India--Delhi",
        href: "place/indiadelhi",
        description: "Place"
    },
    {
        text: "England--Suffolk",
        href: "place/englandsuffolk",
        description: "Place"
    },
    {
        text: "England--Leicester",
        href: "place/englandleicester",
        description: "Place"
    },
    {
        text: "France--Strasbourg",
        href: "place/francestrasbourg",
        description: "Place"
    },
    {
        text: "England--Maidstone",
        href: "place/englandmaidstone",
        description: "Place"
    },
    {
        text: "Wales--Cardiff",
        href: "place/walescardiff",
        description: "Place"
    },
    {
        text: "Wales--Conwy",
        href: "place/walesconwy",
        description: "Place"
    },
    {
        text: "France--Saint-Malo",
        href: "place/francesaintmalo",
        description: "Place"
    },
    {
        text: "England--Margate",
        href: "place/englandmargate",
        description: "Place"
    },
    {
        text: "France--Pierrefonds",
        href: "place/francepierrefonds",
        description: "Place"
    },
    {
        text: "Massachusetts--Boston",
        href: "place/massachusettsboston",
        description: "Place"
    },
    {
        text: "Scotland--Island of Arran",
        href: "place/scotlandisland_of_arran",
        description: "Place"
    },
    {
        text: "Norway",
        href: "place/norway",
        description: "Place"
    },
    {
        text: "India--Agra",
        href: "place/indiaagra",
        description: "Place"
    },
    {
        text: "England--Lowestoft",
        href: "place/englandlowestoft",
        description: "Place"
    },
    {
        text: "England--Newcastle upon Tyne",
        href: "place/englandnewcastle_upon_tyne",
        description: "Place"
    },
    {
        text: "France--Bordeaux (Nouvelle-Aquitaine)",
        href: "place/francebordeaux",
        description: "Place"
    },
    {
        text: "Wales--Llandudno",
        href: "place/walesllandudno",
        description: "Place"
    },
    {
        text: "England--Winchester",
        href: "place/englandwinchester",
        description: "Place"
    },
    {
        text: "England--Bridlington",
        href: "place/englandbridlington",
        description: "Place"
    },
    {
        text: "Morocco",
        href: "place/morocco",
        description: "Place"
    },
    {
        text: "England--Worcestershire",
        href: "place/englandworcestershire",
        description: "Place"
    },
    {
        text: "England--Staffordshire",
        href: "place/englandstaffordshire",
        description: "Place"
    },
    {
        text: "Scotland--Dundee",
        href: "place/scotlanddundee",
        description: "Place"
    },
    {
        text: "Scotland--Glencoe (Village)",
        href: "place/scotlandglencoe",
        description: "Place"
    },
    {
        text: "Scotland--Highlands",
        href: "place/scotlandhighlands",
        description: "Place"
    },
    {
        text: "Scotland--Aberdeenshire",
        href: "place/scotlandaberdeenshire",
        description: "Place"
    },
    {
        text: "India--Kolkata",
        href: "place/indiakolkata",
        description: "Place"
    },
    {
        text: "England--Dove Dale (Derbyshire and Staffordshire)",
        href: "place/englanddove_dale",
        description: "Place"
    },
    {
        text: "England--Weston-super-Mare",
        href: "place/englandwestonsupermare",
        description: "Place"
    },
    {
        text: "Turkey--Istanbul",
        href: "place/turkeyistanbul",
        description: "Place"
    },
    {
        text: "England--Dover",
        href: "place/englanddover",
        description: "Place"
    },
    {
        text: "Wales--Snowdon",
        href: "place/walessnowdon",
        description: "Place"
    },
    {
        text: "England--Bedfordshire",
        href: "place/englandbedfordshire",
        description: "Place"
    },
    {
        text: "Sri Lanka",
        href: "place/sri_lanka",
        description: "Place"
    },
    {
        text: "England--Paignton",
        href: "place/englandpaignton",
        description: "Place"
    },
    {
        text: "England--Southwold",
        href: "place/englandsouthwold",
        description: "Place"
    },
    {
        text: "England--Sunderland (Tyne and Wear)",
        href: "place/englandsunderland",
        description: "Place"
    },
    {
        text: "France--Biarritz",
        href: "place/francebiarritz",
        description: "Place"
    },
    {
        text: "England--Folkestone",
        href: "place/englandfolkestone",
        description: "Place"
    },
    {
        text: "England--St. Ives (Cornwall)",
        href: "place/englandst_ives",
        description: "Place"
    },
    {
        text: "England--Windsor (Windsor and Maidenhead)",
        href: "place/englandwindsor",
        description: "Place"
    },
    {
        text: "England--Oxfordshire",
        href: "place/englandoxfordshire",
        description: "Place"
    },
    {
        text: "England--Clevedon",
        href: "place/englandclevedon",
        description: "Place"
    },
    {
        text: "Philippines--Manila",
        href: "place/philippinesmanila",
        description: "Place"
    },
    {
        text: "Scotland--Loch Ness",
        href: "place/scotlandloch_ness",
        description: "Place"
    },
    {
        text: "Scotland--Shetland",
        href: "place/scotlandshetland",
        description: "Place"
    },
    {
        text: "England--Carlisle",
        href: "place/englandcarlisle",
        description: "Place"
    },
    {
        text: "California",
        href: "place/california",
        description: "Place"
    },
    {
        text: "England--St. Albans",
        href: "place/englandst_albans",
        description: "Place"
    },
    {
        text: "Scotland--Bute",
        href: "place/scotlandbute",
        description: "Place"
    },
    {
        text: "France--Roquebrune-Cap-Martin",
        href: "place/franceroquebrunecapmartin",
        description: "Place"
    },
    {
        text: "Scotland--Dunoon",
        href: "place/scotlanddunoon",
        description: "Place"
    },
    {
        text: "England--Reading",
        href: "place/englandreading",
        description: "Place"
    },
    {
        text: "England--Kenilworth",
        href: "place/englandkenilworth",
        description: "Place"
    },
    {
        text: "Wales--Snowdonia",
        href: "place/walessnowdonia",
        description: "Place"
    },
    {
        text: "Portugal--Lisbon",
        href: "place/portugallisbon",
        description: "Place"
    },
    {
        text: "Scotland--Aberfeldy",
        href: "place/scotlandaberfeldy",
        description: "Place"
    },
    {
        text: "Scotland--Melrose (Parish)",
        href: "place/scotlandmelrose",
        description: "Place"
    },
    {
        text: "England--Salisbury",
        href: "place/englandsalisbury",
        description: "Place"
    },
    {
        text: "England--Bedford",
        href: "place/englandbedford",
        description: "Place"
    },
    {
        text: "England--Windsor Great Park",
        href: "place/englandwindsor_great_park",
        description: "Place"
    },
    {
        text: "England--Peterborough",
        href: "place/englandpeterborough",
        description: "Place"
    },
    {
        text: "Wales--Swansea",
        href: "place/walesswansea",
        description: "Place"
    },
    {
        text: "New Zealand--Rotorua",
        href: "place/new_zealandrotorua",
        description: "Place"
    },
    {
        text: "England--Aldeburgh",
        href: "place/englandaldeburgh",
        description: "Place"
    },
    {
        text: "England--Ventnor",
        href: "place/englandventnor",
        description: "Place"
    },
    {
        text: "England--Stafford",
        href: "place/englandstafford",
        description: "Place"
    },
    {
        text: "Israel",
        href: "place/israel",
        description: "Place"
    },
    {
        text: "France--Le Havre",
        href: "place/francele_havre",
        description: "Place"
    },
    {
        text: "Russia (Federation)",
        href: "place/russia",
        description: "Place"
    },
    {
        text: "Algeria",
        href: "place/algeria",
        description: "Place"
    },
    {
        text: "England--Tewkesbury",
        href: "place/englandtewkesbury",
        description: "Place"
    },
    {
        text: "England--Wells",
        href: "place/englandwells",
        description: "Place"
    },
    {
        text: "England--Sonning",
        href: "place/englandsonning",
        description: "Place"
    },
    {
        text: "France--Carcassonne",
        href: "place/francecarcassonne",
        description: "Place"
    },
    {
        text: "England--Malvern Hills",
        href: "place/englandmalvern_hills",
        description: "Place"
    },
    {
        text: "Scotland--Callander",
        href: "place/scotlandcallander",
        description: "Place"
    },
    {
        text: "England--Arundel",
        href: "place/englandarundel",
        description: "Place"
    },
    {
        text: "England--Tunbridge Wells",
        href: "place/englandtunbridge_wells",
        description: "Place"
    },
    {
        text: "Great Britain Miscellaneous Island Dependencies--Douglas (Isle of Man)",
        href: "place/great_britain_miscellaneous_island_dependenciesdouglas",
        description: "Place"
    },
    {
        text: "Great Britain",
        href: "place/great_britain",
        description: "Place"
    },
    {
        text: "Argentina--Buenos Aires",
        href: "place/argentinabuenos_aires",
        description: "Place"
    },
    {
        text: "Scotland--St. Andrews",
        href: "place/scotlandst_andrews",
        description: "Place"
    },
    {
        text: "China--Shanghai",
        href: "place/chinashanghai",
        description: "Place"
    },
    {
        text: "England--Lake District",
        href: "place/englandlake_district",
        description: "Place"
    },
    {
        text: "Mexico",
        href: "place/mexico",
        description: "Place"
    },
    {
        text: "France--Beaulieu-sur-Mer",
        href: "place/francebeaulieusurmer",
        description: "Place"
    },
    {
        text: "Scotland--Helensburgh",
        href: "place/scotlandhelensburgh",
        description: "Place"
    },
    {
        text: "China",
        href: "place/china",
        description: "Place"
    },
    {
        text: "England--Hereford",
        href: "place/englandhereford",
        description: "Place"
    },
    {
        text: "England--Newquay",
        href: "place/englandnewquay",
        description: "Place"
    },
    {
        text: "England--Wiltshire",
        href: "place/englandwiltshire",
        description: "Place"
    },
    {
        text: "India--Bangalore",
        href: "place/indiabangalore",
        description: "Place"
    },
    {
        text: "England--Cheshire",
        href: "place/englandcheshire",
        description: "Place"
    },
    {
        text: "Singapore",
        href: "place/singapore",
        description: "Place"
    },
    {
        text: "England--Gibraltar Point National Nature Reserve",
        href: "place/englandgibraltar_point_national_nature_reserve",
        description: "Place"
    },
    {
        text: "China--Hong Kong",
        href: "place/chinahong_kong",
        description: "Place"
    },
    {
        text: "Pakistan--Lahore",
        href: "place/pakistanlahore",
        description: "Place"
    },
    {
        text: "India--Jaypur (Koraput District)",
        href: "place/indiajaypur",
        description: "Place"
    },
    {
        text: "England--Swanage",
        href: "place/englandswanage",
        description: "Place"
    },
    {
        text: "Wales--Caernarfon",
        href: "place/walescaernarfon",
        description: "Place"
    },
    {
        text: "England--Ripon",
        href: "place/englandripon",
        description: "Place"
    },
    {
        text: "Scotland--Loch Katrine",
        href: "place/scotlandloch_katrine",
        description: "Place"
    },
    {
        text: "Wales--Tenby",
        href: "place/walestenby",
        description: "Place"
    },
    {
        text: "Scotland--Stonehaven",
        href: "place/scotlandstonehaven",
        description: "Place"
    },
    {
        text: "Scotland--Ayr",
        href: "place/scotlandayr",
        description: "Place"
    },
    {
        text: "France--Chantilly",
        href: "place/francechantilly",
        description: "Place"
    },
    {
        text: "England--Leeds",
        href: "place/englandleeds",
        description: "Place"
    },
    {
        text: "Scotland--Inveraray",
        href: "place/scotlandinveraray",
        description: "Place"
    },
    {
        text: "England--Bristol--Clifton",
        href: "place/englandbristolclifton",
        description: "Place"
    },
    {
        text: "England--New Brighton (Wirral)",
        href: "place/englandnew_brighton",
        description: "Place"
    },
    {
        text: "Scotland--Dunkeld",
        href: "place/scotlanddunkeld",
        description: "Place"
    },
    {
        text: "Italy--Naples",
        href: "place/italynaples",
        description: "Place"
    },
    {
        text: "Middle East--Palestine",
        href: "place/middle_eastpalestine",
        description: "Place"
    },
    {
        text: "New Zealand--Christchurch",
        href: "place/new_zealandchristchurch",
        description: "Place"
    },
    {
        text: "Scotland--Ross-Shire",
        href: "place/scotlandrossshire",
        description: "Place"
    },
    {
        text: "England--Middlesex",
        href: "place/englandmiddlesex",
        description: "Place"
    },
    {
        text: "Scotland--Lochearnhead",
        href: "place/scotlandlochearnhead",
        description: "Place"
    },
    {
        text: "Scotland--Caithness",
        href: "place/scotlandcaithness",
        description: "Place"
    },
    {
        text: "England--Clacton-on-Sea",
        href: "place/englandclactononsea",
        description: "Place"
    },
    {
        text: "Pennsylvania--Philadelphia",
        href: "place/pennsylvaniaphiladelphia",
        description: "Place"
    },
    {
        text: "England--Westcliff-on-Sea",
        href: "place/englandwestcliffonsea",
        description: "Place"
    },
    {
        text: "Scotland--Greenock",
        href: "place/scotlandgreenock",
        description: "Place"
    },
    {
        text: "Wales--Cardiganshire",
        href: "place/walescardiganshire",
        description: "Place"
    },
    {
        text: "England--Ipswich",
        href: "place/englandipswich",
        description: "Place"
    },
    {
        text: "Wales--Llanberis",
        href: "place/walesllanberis",
        description: "Place"
    },
    {
        text: "England--Ely",
        href: "place/englandely",
        description: "Place"
    },
    {
        text: "New Zealand--Auckland",
        href: "place/new_zealandauckland",
        description: "Place"
    },
    {
        text: "England--Dorchester (Dorset)",
        href: "place/englanddorchester",
        description: "Place"
    },
    {
        text: "France--Boulogne-sur-Mer",
        href: "place/franceboulognesurmer",
        description: "Place"
    },
    {
        text: "New Jersey--Asbury Park",
        href: "place/new_jerseyasbury_park",
        description: "Place"
    },
    {
        text: "Scotland--Sutherland",
        href: "place/scotlandsutherland",
        description: "Place"
    },
    {
        text: "Scotland--Ben Nevis",
        href: "place/scotlandben_nevis",
        description: "Place"
    },
    {
        text: "Wales--Rhyl",
        href: "place/walesrhyl",
        description: "Place"
    },
    {
        text: "India--Kanpur",
        href: "place/indiakanpur",
        description: "Place"
    },
    {
        text: "Connecticut--New London",
        href: "place/connecticutnew_london",
        description: "Place"
    },
    {
        text: "England--Rochdale",
        href: "place/englandrochdale",
        description: "Place"
    },
    {
        text: "England--Wye",
        href: "place/englandwye",
        description: "Place"
    },
    {
        text: "Scotland--Ardrossan",
        href: "place/scotlandardrossan",
        description: "Place"
    },
    {
        text: "Wales--Aberystwyth",
        href: "place/walesaberystwyth",
        description: "Place"
    },
    {
        text: "England--Staines",
        href: "place/englandstaines",
        description: "Place"
    },
    {
        text: "England--Huntingdonshire",
        href: "place/englandhuntingdonshire",
        description: "Place"
    },
    {
        text: "Scotland--Loch Earn",
        href: "place/scotlandloch_earn",
        description: "Place"
    },
    {
        text: "India--Lucknow",
        href: "place/indialucknow",
        description: "Place"
    },
    {
        text: "England--Guildford",
        href: "place/englandguildford",
        description: "Place"
    },
    {
        text: "Australia--Adelaide",
        href: "place/australiaadelaide",
        description: "Place"
    },
    {
        text: "England--Rye",
        href: "place/englandrye",
        description: "Place"
    },
    {
        text: "England--London--Greenwich",
        href: "place/englandlondongreenwich",
        description: "Place"
    },
    {
        text: "Russia (Federation)--Saint Petersburg",
        href: "place/russia_federationsaint_petersburg",
        description: "Place"
    },
    {
        text: "Great Britain Miscellaneous Island Dependencies--Sark (Guernsey)",
        href: "place/great_britain_miscellaneous_island_dependenciessark",
        description: "Place"
    },
    {
        text: "England--Gravesend (Kent)",
        href: "place/englandgravesend",
        description: "Place"
    },
    {
        text: "England--Barnstaple",
        href: "place/englandbarnstaple",
        description: "Place"
    },
    {
        text: "England--Ludlow",
        href: "place/englandludlow",
        description: "Place"
    },
    {
        text: "Russia (Federation)--Moscow",
        href: "place/russia_federationmoscow",
        description: "Place"
    },
    {
        text: "Norway--Odda",
        href: "place/norwayodda",
        description: "Place"
    },
    {
        text: "Scotland--Perth",
        href: "place/scotlandperth",
        description: "Place"
    },
    {
        text: "New Jersey--Atlantic City",
        href: "place/new_jerseyatlantic_city",
        description: "Place"
    },
    {
        text: "Scotland--Blair Atholl",
        href: "place/scotlandblair_atholl",
        description: "Place"
    },
    {
        text: "Russia (Federation)--Siberia",
        href: "place/russia_federationsiberia",
        description: "Place"
    },
    {
        text: "New Jersey--Ridgewood",
        href: "place/new_jerseyridgewood",
        description: "Place"
    },
    {
        text: "England--Bakewell",
        href: "place/englandbakewell",
        description: "Place"
    },
    {
        text: "England--Lynton",
        href: "place/englandlynton",
        description: "Place"
    },
    {
        text: "England--Truro",
        href: "place/englandtruro",
        description: "Place"
    },
    {
        text: "England--Bolton (West Yorkshire)",
        href: "place/englandbolton",
        description: "Place"
    },
    {
        text: "Scotland--Pitlochry",
        href: "place/scotlandpitlochry",
        description: "Place"
    },
    {
        text: "Scotland--Garelochhead",
        href: "place/scotlandgarelochhead",
        description: "Place"
    },
    {
        text: "Scotland--Iona",
        href: "place/scotlandiona",
        description: "Place"
    },
    {
        text: "Wales--Llanelli",
        href: "place/walesllanelli",
        description: "Place"
    },
    {
        text: "England--Bognor Regis",
        href: "place/englandbognor_regis",
        description: "Place"
    },
    {
        text: "Italy--Genoa",
        href: "place/italygenoa",
        description: "Place"
    },
    {
        text: "New Zealand--Wellington",
        href: "place/new_zealandwellington",
        description: "Place"
    },
    {
        text: "New Zealand--Dunedin",
        href: "place/new_zealanddunedin",
        description: "Place"
    },
    {
        text: "England--The Broads",
        href: "place/englandthe_broads",
        description: "Place"
    },
    {
        text: "England--Derby",
        href: "place/englandderby",
        description: "Place"
    },
    {
        text: "Wales--Ceredigion",
        href: "place/walesceredigion",
        description: "Place"
    },
    {
        text: "England--Banbury (Oxfordshire)",
        href: "place/englandbanbury",
        description: "Place"
    },
    {
        text: "England--Malvern",
        href: "place/englandmalvern",
        description: "Place"
    },
    {
        text: "England--Lake Ullswater",
        href: "place/englandlake_ullswater",
        description: "Place"
    },
    {
        text: "England--Huddersfield",
        href: "place/englandhuddersfield",
        description: "Place"
    },
    {
        text: "Scotland--Glen Nevis",
        href: "place/scotlandglen_nevis",
        description: "Place"
    },
    {
        text: "Czech Republic",
        href: "place/czech_republic",
        description: "Place"
    },
    {
        text: "Fiji",
        href: "place/fiji",
        description: "Place"
    },
    {
        text: "England--Aylesbury",
        href: "place/englandaylesbury",
        description: "Place"
    },
    {
        text: "England--West Kirby",
        href: "place/englandwest_kirby",
        description: "Place"
    },
    {
        text: "England--Walton on the Naze",
        href: "place/englandwalton_on_the_naze",
        description: "Place"
    },
    {
        text: "England--Matlock",
        href: "place/englandmatlock",
        description: "Place"
    },
    {
        text: "Wales--Builth Wells",
        href: "place/walesbuilth_wells",
        description: "Place"
    },
    {
        text: "England--Bradford (West Yorkshire)",
        href: "place/englandbradford",
        description: "Place"
    },
    {
        text: "England--Shanklin",
        href: "place/englandshanklin",
        description: "Place"
    },
    {
        text: "England--Aldershot",
        href: "place/englandaldershot",
        description: "Place"
    },
    {
        text: "England--Worthing",
        href: "place/englandworthing",
        description: "Place"
    },
    {
        text: "Hawaii",
        href: "place/hawaii",
        description: "Place"
    },
    {
        text: "England--Welford-on-Avon",
        href: "place/englandwelfordonavon",
        description: "Place"
    },
    {
        text: "Scotland--Arbroath",
        href: "place/scotlandarbroath",
        description: "Place"
    },
    {
        text: "England--Gainsborough",
        href: "place/englandgainsborough",
        description: "Place"
    },
    {
        text: "Ukraine--Mukacheve",
        href: "place/ukrainemukacheve",
        description: "Place"
    },
    {
        text: "England--Broadstairs",
        href: "place/englandbroadstairs",
        description: "Place"
    },
    {
        text: "Malaysia--Pinang",
        href: "place/malaysiapinang",
        description: "Place"
    },
    {
        text: "England--Langdale End",
        href: "place/englandlangdale_end",
        description: "Place"
    },
    {
        text: "England--Freshwater",
        href: "place/englandfreshwater",
        description: "Place"
    },
    {
        text: "England--Bushey",
        href: "place/englandbushey",
        description: "Place"
    },
    {
        text: "Wales--Barry",
        href: "place/walesbarry",
        description: "Place"
    },
    {
        text: "Arctic Regions",
        href: "place/arctic_regions",
        description: "Place"
    },
    {
        text: "Portugal",
        href: "place/portugal",
        description: "Place"
    },
    {
        text: "England--Lincolnshire",
        href: "place/englandlincolnshire",
        description: "Place"
    },
    {
        text: "Mexico--Cuernavaca",
        href: "place/mexicocuernavaca",
        description: "Place"
    },
    {
        text: "Morocco--Marrakech",
        href: "place/moroccomarrakech",
        description: "Place"
    },
    {
        text: "Scotland--Fife",
        href: "place/scotlandfife",
        description: "Place"
    },
    {
        text: "Georgia",
        href: "place/georgia",
        description: "Place"
    },
    {
        text: "New Jersey",
        href: "place/new_jersey",
        description: "Place"
    },
    {
        text: "Scotland--Fairlie",
        href: "place/scotlandfairlie",
        description: "Place"
    },
    {
        text: "Poland--Gdansk",
        href: "place/polandgdansk",
        description: "Place"
    },
    {
        text: "England--Hertfordshire",
        href: "place/englandhertfordshire",
        description: "Place"
    },
    {
        text: "England--Northwich",
        href: "place/englandnorthwich",
        description: "Place"
    },
    {
        text: "England--Coventry",
        href: "place/englandcoventry",
        description: "Place"
    },
    {
        text: "Scotland--Aberfoyle",
        href: "place/scotlandaberfoyle",
        description: "Place"
    },
    {
        text: "Scotland--North Berwick",
        href: "place/scotlandnorth_berwick",
        description: "Place"
    },
    {
        text: "Wales--Neath Port Talbot",
        href: "place/walesneath_port_talbot",
        description: "Place"
    },
    {
        text: "England--Croydon",
        href: "place/englandcroydon",
        description: "Place"
    },
    {
        text: "Scotland--Stirlingshire",
        href: "place/scotlandstirlingshire",
        description: "Place"
    },
    {
        text: "England--Luton",
        href: "place/englandluton",
        description: "Place"
    },
    {
        text: "England--Shropshire",
        href: "place/englandshropshire",
        description: "Place"
    },
    {
        text: "England--St. Helens (Merseyside)",
        href: "place/englandst_helens",
        description: "Place"
    },
    {
        text: "Wales--Penarth",
        href: "place/walespenarth",
        description: "Place"
    },
    {
        text: "England--Abingdon",
        href: "place/englandabingdon",
        description: "Place"
    },
    {
        text: "France--Villefranche-sur-Mer",
        href: "place/francevillefranchesurmer",
        description: "Place"
    },
    {
        text: "Scotland--Loch Etive",
        href: "place/scotlandloch_etive",
        description: "Place"
    },
    {
        text: "England--London--Chelsea",
        href: "place/englandlondonchelsea",
        description: "Place"
    },
    {
        text: "Wales--Capel Curig",
        href: "place/walescapel_curig",
        description: "Place"
    },
    {
        text: "England--Penzance",
        href: "place/englandpenzance",
        description: "Place"
    },
    {
        text: "Wales--Dolgellau",
        href: "place/walesdolgellau",
        description: "Place"
    },
    {
        text: "France--Pyrénées-Atlantiques",
        href: "place/francepyreneesatlantiques",
        description: "Place"
    },
    {
        text: "England--Epping Forest",
        href: "place/englandepping_forest",
        description: "Place"
    },
    {
        text: "England--St. Leonards (East Sussex)",
        href: "place/englandst_leonards",
        description: "Place"
    },
    {
        text: "England--Knaresborough",
        href: "place/englandknaresborough",
        description: "Place"
    },
    {
        text: "New Jersey--Lake Hopatcong (Lake)",
        href: "place/new_jerseylake_hopatcong",
        description: "Place"
    },
    {
        text: "France--Toulon",
        href: "place/francetoulon",
        description: "Place"
    },
    {
        text: "France--Ossau Valley",
        href: "place/franceossau_valley",
        description: "Place"
    },
    {
        text: "England--Helston",
        href: "place/englandhelston",
        description: "Place"
    },
    {
        text: "Wales--Neath",
        href: "place/walesneath",
        description: "Place"
    },
    {
        text: "England--Maidenhead",
        href: "place/englandmaidenhead",
        description: "Place"
    },
    {
        text: "Syria",
        href: "place/syria",
        description: "Place"
    },
    {
        text: "Georgia--Marietta",
        href: "place/georgiamarietta",
        description: "Place"
    },
    {
        text: "India--Mysore",
        href: "place/indiamysore",
        description: "Place"
    },
    {
        text: "England--Tavistock",
        href: "place/englandtavistock",
        description: "Place"
    },
    {
        text: "England--Stourbridge",
        href: "place/englandstourbridge",
        description: "Place"
    },
    {
        text: "England--Reigate",
        href: "place/englandreigate",
        description: "Place"
    },
    {
        text: "Sri Lanka--Kandy",
        href: "place/sri_lankakandy",
        description: "Place"
    },
    {
        text: "Puerto Rico--San Juan",
        href: "place/puerto_ricosan_juan",
        description: "Place"
    },
    {
        text: "Georgia--Decatur",
        href: "place/georgiadecatur",
        description: "Place"
    },
    {
        text: "New York (State)--Poughkeepsie",
        href: "place/new_york_statepoughkeepsie",
        description: "Place"
    },
    {
        text: "England--Horning",
        href: "place/englandhorning",
        description: "Place"
    },
    {
        text: "Puerto Rico",
        href: "place/puerto_rico",
        description: "Place"
    },
    {
        text: "England--Tonbridge",
        href: "place/englandtonbridge",
        description: "Place"
    },
    {
        text: "Scotland--Killin",
        href: "place/scotlandkillin",
        description: "Place"
    },
    {
        text: "England--Halifax",
        href: "place/englandhalifax",
        description: "Place"
    },
    {
        text: "France--Brittany",
        href: "place/francebrittany",
        description: "Place"
    },
    {
        text: "Scotland--Berwickshire",
        href: "place/scotlandberwickshire",
        description: "Place"
    },
    {
        text: "England--Bray",
        href: "place/englandbray",
        description: "Place"
    },
    {
        text: "Israel--Galilee",
        href: "place/israelgalilee",
        description: "Place"
    },
    {
        text: "England--Winchelsea",
        href: "place/englandwinchelsea",
        description: "Place"
    },
    {
        text: "England--Porlock",
        href: "place/englandporlock",
        description: "Place"
    },
    {
        text: "Scotland--Loch Oich",
        href: "place/scotlandloch_oich",
        description: "Place"
    },
    {
        text: "England--Uttoxeter",
        href: "place/englanduttoxeter",
        description: "Place"
    },
    {
        text: "Great Britain--River Wye Valley",
        href: "place/great_britainriver_wye_valley",
        description: "Place"
    },
    {
        text: "England--Middlesbrough",
        href: "place/englandmiddlesbrough",
        description: "Place"
    },
    {
        text: "Scotland--Arrochar",
        href: "place/scotlandarrochar",
        description: "Place"
    },
    {
        text: "Algeria--Biskra (Province)",
        href: "place/algeriabiskra",
        description: "Place"
    },
    {
        text: "England--Shottery",
        href: "place/englandshottery",
        description: "Place"
    },
    {
        text: "Wales--Snowdonia National Park",
        href: "place/walessnowdonia_national_park",
        description: "Place"
    },
    {
        text: "England--Barrow-in-Furness",
        href: "place/englandbarrowinfurness",
        description: "Place"
    },
    {
        text: "Wales--Porthcawl",
        href: "place/walesporthcawl",
        description: "Place"
    },
    {
        text: "Connecticut--Niantic",
        href: "place/connecticutniantic",
        description: "Place"
    },
    {
        text: "Scotland--Gourock",
        href: "place/scotlandgourock",
        description: "Place"
    },
    {
        text: "Scotland--Ballater",
        href: "place/scotlandballater",
        description: "Place"
    },
    {
        text: "Italy--Baveno",
        href: "place/italybaveno",
        description: "Place"
    },
    {
        text: "Massachusetts--Shelburne Falls",
        href: "place/massachusettsshelburne_falls",
        description: "Place"
    },
    {
        text: "England--Walberswick",
        href: "place/englandwalberswick",
        description: "Place"
    },
    {
        text: "Wales--Llandrindod",
        href: "place/walesllandrindod",
        description: "Place"
    },
    {
        text: "England--Deal",
        href: "place/englanddeal",
        description: "Place"
    },
    {
        text: "Scotland--Hebrides",
        href: "place/scotlandhebrides",
        description: "Place"
    },
    {
        text: "Scotland--Staffa",
        href: "place/scotlandstaffa",
        description: "Place"
    },
    {
        text: "Scotland--Moray",
        href: "place/scotlandmoray",
        description: "Place"
    },
    {
        text: "Scotland--Kingussie",
        href: "place/scotlandkingussie",
        description: "Place"
    },
    {
        text: "Scotland--Argyll and Bute",
        href: "place/scotlandargyll_and_bute",
        description: "Place"
    },
    {
        text: "Scotland--Caledonian Canal",
        href: "place/scotlandcaledonian_canal",
        description: "Place"
    },
    {
        text: "Scotland--Fort Augustus",
        href: "place/scotlandfort_augustus",
        description: "Place"
    },
    {
        text: "England--Ilkley",
        href: "place/englandilkley",
        description: "Place"
    },
    {
        text: "New Jersey--Spring Lake",
        href: "place/new_jerseyspring_lake",
        description: "Place"
    },
    {
        text: "California--Los Angeles",
        href: "place/californialos_angeles",
        description: "Place"
    },
    {
        text: "England--Goring",
        href: "place/englandgoring",
        description: "Place"
    },
    {
        text: "England--Fleetwood",
        href: "place/englandfleetwood",
        description: "Place"
    },
    {
        text: "France--Riviera",
        href: "place/franceriviera",
        description: "Place"
    },
    {
        text: "Brazil--Bahia",
        href: "place/brazilbahia",
        description: "Place"
    },
    {
        text: "Brazil--Rio de Janeiro",
        href: "place/brazilrio_de_janeiro",
        description: "Place"
    },
    {
        text: "Wales--Beddgelert",
        href: "place/walesbeddgelert",
        description: "Place"
    },
    {
        text: "Scotland--Luss",
        href: "place/scotlandluss",
        description: "Place"
    },
    {
        text: "England--Newlyn",
        href: "place/englandnewlyn",
        description: "Place"
    },
    {
        text: "England--Glastonbury",
        href: "place/englandglastonbury",
        description: "Place"
    },
    {
        text: "Chile",
        href: "place/chile",
        description: "Place"
    },
    {
        text: "England--Warrington",
        href: "place/englandwarrington",
        description: "Place"
    },
    {
        text: "England--Bonchurch (Isle of Wight)",
        href: "place/englandbonchurch",
        description: "Place"
    },
    {
        text: "Scotland--Awe, Loch",
        href: "place/scotlandawe_loch",
        description: "Place"
    },
    {
        text: "Mexico--Mexico City",
        href: "place/mexicomexico_city",
        description: "Place"
    },
    {
        text: "England--Harwich",
        href: "place/englandharwich",
        description: "Place"
    },
    {
        text: "India--Benares",
        href: "place/indiabenares",
        description: "Place"
    },
    {
        text: "West Indies",
        href: "place/west_indies",
        description: "Place"
    },
    {
        text: "India--Chennai",
        href: "place/indiachennai",
        description: "Place"
    },
    {
        text: "England--Matlock Bath",
        href: "place/englandmatlock_bath",
        description: "Place"
    },
    {
        text: "England--Barnsley (Gloucestershire)",
        href: "place/englandbarnsley",
        description: "Place"
    },
    {
        text: "England--Berwick-upon-Tweed",
        href: "place/englandberwickupontweed",
        description: "Place"
    },
    {
        text: "Scotland--Elie",
        href: "place/scotlandelie",
        description: "Place"
    },
    {
        text: "Great Britain Miscellaneous Island Dependencies--Channel Islands",
        href: "place/great_britain_miscellaneous_island_dependencieschannel_islands",
        description: "Place"
    },
    {
        text: "England--Launceston",
        href: "place/englandlaunceston",
        description: "Place"
    },
    {
        text: "England--Swindon",
        href: "place/englandswindon",
        description: "Place"
    },
    {
        text: "England--Colchester",
        href: "place/englandcolchester",
        description: "Place"
    },
    {
        text: "England--London--Kew",
        href: "place/englandlondonkew",
        description: "Place"
    },
    {
        text: "New York (State)--New York--Luna Park",
        href: "place/new_york_statenew_yorkluna_park",
        description: "Place"
    },
    {
        text: "England--Virginia Water (Lake)",
        href: "place/englandvirginia_water",
        description: "Place"
    },
    {
        text: "Scotland--Highland",
        href: "place/scotlandhighland",
        description: "Place"
    },
    {
        text: "England--Lewes",
        href: "place/englandlewes",
        description: "Place"
    },
    {
        text: "England--Newhaven",
        href: "place/englandnewhaven",
        description: "Place"
    },
    {
        text: "New Jersey--Wildwood",
        href: "place/new_jerseywildwood",
        description: "Place"
    },
    {
        text: "Poland",
        href: "place/poland",
        description: "Place"
    },
    {
        text: "Scotland--Millport",
        href: "place/scotlandmillport",
        description: "Place"
    },
    {
        text: "England--New Forest (Forest)",
        href: "place/englandnew_forest",
        description: "Place"
    },
    {
        text: "India--Vārānasi (Uttar Pradesh)",
        href: "place/indiavaranasi",
        description: "Place"
    },
    {
        text: "New Jersey--Ocean City",
        href: "place/new_jerseyocean_city",
        description: "Place"
    },
    {
        text: "England--Lancaster",
        href: "place/englandlancaster",
        description: "Place"
    },
    {
        text: "New Zealand",
        href: "place/new_zealand",
        description: "Place"
    },
    {
        text: "England--Ilfracombe",
        href: "place/englandilfracombe",
        description: "Place"
    },
    {
        text: "England--Pangbourne",
        href: "place/englandpangbourne",
        description: "Place"
    },
    {
        text: "England--Hull",
        href: "place/englandhull",
        description: "Place"
    },
    {
        text: "Iowa--Sanborn",
        href: "place/iowasanborn",
        description: "Place"
    },
    {
        text: "Massachusetts--Pittsfield",
        href: "place/massachusettspittsfield",
        description: "Place"
    },
    {
        text: "Colorado--Georgetown Loop Historic Mining and Railroad Park",
        href: "place/coloradogeorgetown_loop_historic_mining_and_railroad_park",
        description: "Place"
    },
    {
        text: "Ireland",
        href: "place/ireland",
        description: "Place"
    },
    {
        text: "Iowa--Grand Mound",
        href: "place/iowagrand_mound",
        description: "Place"
    },
    {
        text: "Indiana--Indianapolis",
        href: "place/indianaindianapolis",
        description: "Place"
    },
    {
        text: "New York (State)--Granville",
        href: "place/new_york_stategranville",
        description: "Place"
    },
    {
        text: "Germany",
        href: "place/germany",
        description: "Place"
    },
    {
        text: "Italy--Bologna",
        href: "place/italybologna",
        description: "Place"
    },
    {
        text: "Colorado--Denver",
        href: "place/coloradodenver",
        description: "Place"
    },
    {
        text: "England--Loughborough",
        href: "place/englandloughborough",
        description: "Place"
    },
    {
        text: "England--Sanderstead",
        href: "place/englandsanderstead",
        description: "Place"
    },
    {
        text: "Wales--Llanrwst",
        href: "place/walesllanrwst",
        description: "Place"
    },
    {
        text: "Scotland--Ayrshire",
        href: "place/scotlandayrshire",
        description: "Place"
    },
    {
        text: "England--England--Cliftonville",
        href: "place/englandenglandcliftonville",
        description: "Place"
    },
    {
        text: "England--Brockenhurst",
        href: "place/englandbrockenhurst",
        description: "Place"
    },
    {
        text: "Scotland--Abbotsford",
        href: "place/scotlandabbotsford",
        description: "Place"
    },
    {
        text: "England--Sutherlandshire",
        href: "place/englandsutherlandshire",
        description: "Place"
    },
    {
        text: "India--Madras",
        href: "place/indiamadras",
        description: "Place"
    },
    {
        text: "England--Shepperton",
        href: "place/englandshepperton",
        description: "Place"
    },
    {
        text: "England--Coltishall",
        href: "place/englandcoltishall",
        description: "Place"
    },
    {
        text: "Scotland--Mauchline",
        href: "place/scotlandmauchline",
        description: "Place"
    },
    {
        text: "France--Lourdes",
        href: "place/francelourdes",
        description: "Place"
    },
    {
        text: "England--Gloucestershire",
        href: "place/englandgloucestershire",
        description: "Place"
    },
    {
        text: "Scotland--Inverness-shire",
        href: "place/scotlandinvernessshire",
        description: "Place"
    },
    {
        text: "England--Leicestershire",
        href: "place/englandleicestershire",
        description: "Place"
    },
    {
        text: "Wales--Chepstow",
        href: "place/waleschepstow",
        description: "Place"
    },
    {
        text: "Hayastan",
        href: "place/hayastan",
        description: "Place"
    },
    {
        text: "Scotland--Alloway",
        href: "place/scotlandalloway",
        description: "Place"
    },
    {
        text: "Puerto Rico--Cayey",
        href: "place/puerto_ricocayey",
        description: "Place"
    },
    {
        text: "Italy--Trentino-Alto Adige",
        href: "place/italytrentinoalto_adige",
        description: "Place"
    },
    {
        text: "India--Udaipur",
        href: "place/indiaudaipur",
        description: "Place"
    },
    {
        text: "Norway--Sogn",
        href: "place/norwaysogn",
        description: "Place"
    },
    {
        text: "Portugal--Madeira",
        href: "place/portugalmadeira",
        description: "Place"
    },
    {
        text: "England--Filey",
        href: "place/englandfiley",
        description: "Place"
    },
    {
        text: "Israel--Nazareth",
        href: "place/israelnazareth",
        description: "Place"
    },
    {
        text: "Wales--Bethesda (Gwynedd)",
        href: "place/walesbethesda",
        description: "Place"
    },
    {
        text: "England--Skegness",
        href: "place/englandskegness",
        description: "Place"
    },
    {
        text: "Scotland--Inverurie",
        href: "place/scotlandinverurie",
        description: "Place"
    },
    {
        text: "England--Totnes",
        href: "place/englandtotnes",
        description: "Place"
    },
    {
        text: "Wales--Menai Straits",
        href: "place/walesmenai_straits",
        description: "Place"
    },
    {
        text: "Alberta",
        href: "place/alberta",
        description: "Place"
    },
    {
        text: "Scotland--Peebles",
        href: "place/scotlandpeebles",
        description: "Place"
    },
    {
        text: "England--Saltburn-by-the-Sea",
        href: "place/englandsaltburnbythesea",
        description: "Place"
    },
    {
        text: "England--London--Chingford",
        href: "place/englandlondonchingford",
        description: "Place"
    },
    {
        text: "Massachusetts--Brookfield",
        href: "place/massachusettsbrookfield",
        description: "Place"
    },
    {
        text: "Italy--Sorrento",
        href: "place/italysorrento",
        description: "Place"
    },
    {
        text: "Italy--Capri",
        href: "place/italycapri",
        description: "Place"
    },
    {
        text: "Maine--Kittery",
        href: "place/mainekittery",
        description: "Place"
    },
    {
        text: "Massachusetts--Fitchburg",
        href: "place/massachusettsfitchburg",
        description: "Place"
    },
    {
        text: "Maine--North Berwick",
        href: "place/mainenorth_berwick",
        description: "Place"
    },
    {
        text: "Massachusetts--Shirley",
        href: "place/massachusettsshirley",
        description: "Place"
    },
    {
        text: "England--Poole",
        href: "place/englandpoole",
        description: "Place"
    },
    {
        text: "England--Southsea",
        href: "place/englandsouthsea",
        description: "Place"
    },
    {
        text: "Massachusetts--Ayer",
        href: "place/massachusettsayer",
        description: "Place"
    },
    {
        text: "Massachusetts--Winchendon",
        href: "place/massachusettswinchendon",
        description: "Place"
    },
    {
        text: "England--Great Missenden",
        href: "place/englandgreat_missenden",
        description: "Place"
    },
    {
        text: "England--Wendover",
        href: "place/englandwendover",
        description: "Place"
    },
    {
        text: "Massachusetts--West Townsend",
        href: "place/massachusettswest_townsend",
        description: "Place"
    },
    {
        text: "Western Australia--Perth",
        href: "place/western_australiaperth",
        description: "Place"
    },
    {
        text: "Morocco--Mogador",
        href: "place/moroccomogador",
        description: "Place"
    },
    {
        text: "Italy--Positano",
        href: "place/italypositano",
        description: "Place"
    },
    {
        text: "Sri Lanka--Mahaweli River Region",
        href: "place/sri_lankamahaweli_river_region",
        description: "Place"
    },
    {
        text: "England--Epping",
        href: "place/englandepping",
        description: "Place"
    },
    {
        text: "England--Leighton Buzzard",
        href: "place/englandleighton_buzzard",
        description: "Place"
    },
    {
        text: "Alps",
        href: "place/alps",
        description: "Place"
    },
    {
        text: "Scotland--Island of Bute",
        href: "place/scotlandisland_of_bute",
        description: "Place"
    },
    {
        text: "England--Hunts",
        href: "place/englandhunts",
        description: "Place"
    },
    {
        text: "Scotland--Loch Long",
        href: "place/scotlandloch_long",
        description: "Place"
    },
    {
        text: "Ontario--Niagara Falls",
        href: "place/ontarioniagara_falls",
        description: "Place"
    },
    {
        text: "Hawaii--Honolulu",
        href: "place/hawaiihonolulu",
        description: "Place"
    },
    {
        text: "Scotland--Colonsay",
        href: "place/scotlandcolonsay",
        description: "Place"
    },
    {
        text: "England--Land's End Peninsula",
        href: "place/englandlands_end_peninsula",
        description: "Place"
    },
    {
        text: "England--Wallingford",
        href: "place/englandwallingford",
        description: "Place"
    },
    {
        text: "Israel--Tiberias",
        href: "place/israeltiberias",
        description: "Place"
    },
    {
        text: "United Kingdom",
        href: "place/united_kingdom",
        description: "Place"
    },
    {
        text: "New York (State)--Nyack",
        href: "place/new_york_statenyack",
        description: "Place"
    },
    {
        text: "New York (State)--Dolgeville",
        href: "place/new_york_statedolgeville",
        description: "Place"
    },
    {
        text: "Mexico--Orizaba (Veracruz-Llave)",
        href: "place/mexicoorizaba",
        description: "Place"
    },
    {
        text: "North Wales",
        href: "place/north_wales",
        description: "Place"
    },
    {
        text: "Georgia--Stone Mountain",
        href: "place/georgiastone_mountain",
        description: "Place"
    },
    {
        text: "England--Newport (Shropshire)",
        href: "place/englandnewport",
        description: "Place"
    },
    {
        text: "England--East Sussex",
        href: "place/englandeast_sussex",
        description: "Place"
    },
    {
        text: "England--Keston",
        href: "place/englandkeston",
        description: "Place"
    },
    {
        text: "England--Lancashire",
        href: "place/englandlancashire",
        description: "Place"
    },
    {
        text: "England--Ashbourne",
        href: "place/englandashbourne",
        description: "Place"
    },
    {
        text: "Puerto Rico--Ponce",
        href: "place/puerto_ricoponce",
        description: "Place"
    },
    {
        text: "England--Skelwith",
        href: "place/englandskelwith",
        description: "Place"
    },
    {
        text: "Scotland--Loch Awe (Argyll and Bute)",
        href: "place/scotlandloch_awe",
        description: "Place"
    },
    {
        text: "England--Chichester",
        href: "place/englandchichester",
        description: "Place"
    },
    {
        text: "Scotland--Dryburgh",
        href: "place/scotlanddryburgh",
        description: "Place"
    },
    {
        text: "Scotland--Hawick",
        href: "place/scotlandhawick",
        description: "Place"
    },
    {
        text: "England--Ringwood",
        href: "place/englandringwood",
        description: "Place"
    },
    {
        text: "New York (State)--Little Falls",
        href: "place/new_york_statelittle_falls",
        description: "Place"
    },
    {
        text: "Tunisia--Tunis",
        href: "place/tunisiatunis",
        description: "Place"
    },
    {
        text: "England--Lichfield",
        href: "place/englandlichfield",
        description: "Place"
    },
    {
        text: "Manitoba--Winnipeg",
        href: "place/manitobawinnipeg",
        description: "Place"
    },
    {
        text: "England--Hindhead",
        href: "place/englandhindhead",
        description: "Place"
    },
    {
        text: "Scotland--Banavie",
        href: "place/scotlandbanavie",
        description: "Place"
    },
    {
        text: "Scotland--Loch Achray",
        href: "place/scotlandloch_achray",
        description: "Place"
    },
    {
        text: "England--Herefordshire",
        href: "place/englandherefordshire",
        description: "Place"
    },
    {
        text: "Wales--Pontypridd",
        href: "place/walespontypridd",
        description: "Place"
    },
    {
        text: "England--Minehead",
        href: "place/englandminehead",
        description: "Place"
    },
    {
        text: "Scotland--Killiecrankie",
        href: "place/scotlandkilliecrankie",
        description: "Place"
    },
    {
        text: "Scotland--Lochaber",
        href: "place/scotlandlochaber",
        description: "Place"
    },
    {
        text: "England--Huntingdon",
        href: "place/englandhuntingdon",
        description: "Place"
    },
    {
        text: "England--Sevenoaks",
        href: "place/englandsevenoaks",
        description: "Place"
    },
    {
        text: "England--Hunstanton",
        href: "place/englandhunstanton",
        description: "Place"
    },
    {
        text: "Ghana",
        href: "place/ghana",
        description: "Place"
    },
    {
        text: "Wales--Llanfair",
        href: "place/walesllanfair",
        description: "Place"
    },
    {
        text: "New York (State)--Long Island",
        href: "place/new_york_statelong_island",
        description: "Place"
    },
    {
        text: "England--Staffordshire Moorlands",
        href: "place/englandstaffordshire_moorlands",
        description: "Place"
    },
    {
        text: "Saint Lucia",
        href: "place/saint_lucia",
        description: "Place"
    },
    {
        text: "New Jersey--Ocean Grove",
        href: "place/new_jerseyocean_grove",
        description: "Place"
    },
    {
        text: "New Jersey--Long Branch",
        href: "place/new_jerseylong_branch",
        description: "Place"
    },
    {
        text: "Scotland--Berwick",
        href: "place/scotlandberwick",
        description: "Place"
    },
    {
        text: "Scotland--Tarbert (Argyll and Bute)",
        href: "place/scotlandtarbert",
        description: "Place"
    },
    {
        text: "New York (State)--Niagara Falls",
        href: "place/new_york_stateniagara_falls",
        description: "Place"
    },
    {
        text: "New York (State)--West Point",
        href: "place/new_york_statewest_point",
        description: "Place"
    },
    {
        text: "England--Bideford",
        href: "place/englandbideford",
        description: "Place"
    },
    {
        text: "Wales--Gwynedd",
        href: "place/walesgwynedd",
        description: "Place"
    },
    {
        text: "England--Boston",
        href: "place/englandboston",
        description: "Place"
    },
    {
        text: "Wisconsin",
        href: "place/wisconsin",
        description: "Place"
    },
    {
        text: "India--Deccan",
        href: "place/indiadeccan",
        description: "Place"
    },
    {
        text: "Italy--San Pietro Infine",
        href: "place/italysan_pietro_infine",
        description: "Place"
    },
    {
        text: "New York (State)--Mohawk River Valley",
        href: "place/new_york_statemohawk_river_valley",
        description: "Place"
    },
    {
        text: "Norway--Hardangervidda",
        href: "place/norwayhardangervidda",
        description: "Place"
    },
    {
        text: "Norway--Nordland fylke",
        href: "place/norwaynordland_fylke",
        description: "Place"
    },
    {
        text: "Iowa--Thayer",
        href: "place/iowathayer",
        description: "Place"
    },
    {
        text: "Iowa--Des Moines",
        href: "place/iowades_moines",
        description: "Place"
    },
    {
        text: "India--Darjeeling",
        href: "place/indiadarjeeling",
        description: "Place"
    },
    {
        text: "Norway--Ulvik (Vestland fylke)",
        href: "place/norwayulvik",
        description: "Place"
    },
    {
        text: "Nova Scotia--Halifax",
        href: "place/nova_scotiahalifax",
        description: "Place"
    },
    {
        text: "Ontario--Kenora (District)",
        href: "place/ontariokenora",
        description: "Place"
    },
    {
        text: "Colorado--Colorado Springs--Garden of the Gods",
        href: "place/coloradocolorado_springsgarden_of_the_gods",
        description: "Place"
    },
    {
        text: "Pennsylvania--Valley Forge",
        href: "place/pennsylvaniavalley_forge",
        description: "Place"
    },
    {
        text: "Norway--North Cape (Magerøy Island)",
        href: "place/norwaynorth_cape",
        description: "Place"
    },
    {
        text: "Italy--Trieste",
        href: "place/italytrieste",
        description: "Place"
    },
    {
        text: "England--Stratford-upon-Avon Region",
        href: "place/englandstratforduponavon_region",
        description: "Place"
    },
    {
        text: "England--Warwick Castle Park",
        href: "place/englandwarwick_castle_park",
        description: "Place"
    },
    {
        text: "Scotland--East Lothian",
        href: "place/scotlandeast_lothian",
        description: "Place"
    },
    {
        text: "England--Teesdale (Valley)",
        href: "place/englandteesdale",
        description: "Place"
    },
    {
        text: "England--Stilton",
        href: "place/englandstilton",
        description: "Place"
    },
    {
        text: "England--Pullborough",
        href: "place/englandpullborough",
        description: "Place"
    },
    {
        text: "England--Prestbury",
        href: "place/englandprestbury",
        description: "Place"
    },
    {
        text: "England--North Yorkshire",
        href: "place/englandnorth_yorkshire",
        description: "Place"
    },
    {
        text: "England--London--Piccadilly",
        href: "place/englandlondonpiccadilly",
        description: "Place"
    },
    {
        text: "Tunisia",
        href: "place/tunisia",
        description: "Place"
    },
    {
        text: "England--London--Isleworth",
        href: "place/englandlondonisleworth",
        description: "Place"
    },
    {
        text: "Scotland--Dundee--Broughty Ferry",
        href: "place/scotlanddundeebroughty_ferry",
        description: "Place"
    },
    {
        text: "England--London--Barnes",
        href: "place/englandlondonbarnes",
        description: "Place"
    },
    {
        text: "England--Olney",
        href: "place/englandolney",
        description: "Place"
    },
    {
        text: "England--Isles of Scilly",
        href: "place/englandisles_of_scilly",
        description: "Place"
    },
    {
        text: "Russia (Federation)--Lake Baikal",
        href: "place/russia_federationlake_baikal",
        description: "Place"
    },
    {
        text: "Iowa--Farley",
        href: "place/iowafarley",
        description: "Place"
    },
    {
        text: "Martinique",
        href: "place/martinique",
        description: "Place"
    },
    {
        text: "Ukraine--Uz︠h︡horod",
        href: "place/ukraineuzhhorod",
        description: "Place"
    },
    {
        text: "Maine--Berwick",
        href: "place/maineberwick",
        description: "Place"
    },
    {
        text: "Massachusetts--Northboro",
        href: "place/massachusettsnorthboro",
        description: "Place"
    },
    {
        text: "Massachusetts--South Lancaster",
        href: "place/massachusettssouth_lancaster",
        description: "Place"
    },
    {
        text: "Ontario",
        href: "place/ontario",
        description: "Place"
    },
    {
        text: "Massachusetts--Winthrop Beach",
        href: "place/massachusettswinthrop_beach",
        description: "Place"
    },
    {
        text: "Wales--Bodelwydden",
        href: "place/walesbodelwydden",
        description: "Place"
    },
    {
        text: "Massachusetts--Hopkinton",
        href: "place/massachusettshopkinton",
        description: "Place"
    },
    {
        text: "Hawaii--Kauai",
        href: "place/hawaiikauai",
        description: "Place"
    },
    {
        text: "Maine--Sanford",
        href: "place/mainesanford",
        description: "Place"
    },
    {
        text: "Maine--Springvale",
        href: "place/mainespringvale",
        description: "Place"
    },
    {
        text: "Maine--South Berwick",
        href: "place/mainesouth_berwick",
        description: "Place"
    },
    {
        text: "Oregon",
        href: "place/oregon",
        description: "Place"
    },
    {
        text: "Virginia--Newport News",
        href: "place/virginianewport_news",
        description: "Place"
    },
    {
        text: "Iowa--Orient",
        href: "place/iowaorient",
        description: "Place"
    },
    {
        text: "England--Elstow",
        href: "place/englandelstow",
        description: "Place"
    },
    {
        text: "England--Thorpe",
        href: "place/englandthorpe",
        description: "Place"
    },
    {
        text: "England--Cawthorne (South Yorkshire)",
        href: "place/englandcawthorne",
        description: "Place"
    },
    {
        text: "Scotland--Agnus",
        href: "place/scotlandagnus",
        description: "Place"
    },
    {
        text: "Scotland--Barra Island",
        href: "place/scotlandbarra_island",
        description: "Place"
    },
    {
        text: "Iowa--Garrison",
        href: "place/iowagarrison",
        description: "Place"
    },
    {
        text: "Scotland--Glen Shee",
        href: "place/scotlandglen_shee",
        description: "Place"
    },
    {
        text: "Scotland--Golspie",
        href: "place/scotlandgolspie",
        description: "Place"
    },
    {
        text: "Scotland--Holy Island",
        href: "place/scotlandholy_island",
        description: "Place"
    },
    {
        text: "England--Hoo (Kent)",
        href: "place/englandhoo",
        description: "Place"
    },
    {
        text: "Scotland--Loch Ussie",
        href: "place/scotlandloch_ussie",
        description: "Place"
    },
    {
        text: "Iowa--Otho",
        href: "place/iowaotho",
        description: "Place"
    },
    {
        text: "Puerto Rico--Mayaguez",
        href: "place/puerto_ricomayaguez",
        description: "Place"
    },
    {
        text: "United States--New York (State)",
        href: "place/united_statesnew_york",
        description: "Place"
    },
    {
        text: "Wales--Lleyn Peninsula",
        href: "place/waleslleyn_peninsula",
        description: "Place"
    },
    {
        text: "Italy--Bellagio",
        href: "place/italybellagio",
        description: "Place"
    },
    {
        text: "Wales--Ogwen Valley",
        href: "place/walesogwen_valley",
        description: "Place"
    },
    {
        text: "Scotland--Rothesay",
        href: "place/scotlandrothesay",
        description: "Place"
    },
    {
        text: "England--Hillingdon--Hayes",
        href: "place/englandhillingdonhayes",
        description: "Place"
    },
    {
        text: "Asia--Khyber Pass",
        href: "place/asiakhyber_pass",
        description: "Place"
    },
    {
        text: "Europe",
        href: "place/europe",
        description: "Place"
    },
    {
        text: "India--Jaipur",
        href: "place/indiajaipur",
        description: "Place"
    },
    {
        text: "India--Bhopal",
        href: "place/indiabhopal",
        description: "Place"
    },
    {
        text: "India--India--Gwalior",
        href: "place/indiaindiagwalior",
        description: "Place"
    },
    {
        text: "India--Indore",
        href: "place/indiaindore",
        description: "Place"
    },
    {
        text: "Scotland--Gretna Green",
        href: "place/scotlandgretna_green",
        description: "Place"
    },
    {
        text: "Ireland--Killarney (Kerry)",
        href: "place/irelandkillarney",
        description: "Place"
    },
    {
        text: "India--Bikaner",
        href: "place/indiabikaner",
        description: "Place"
    },
    {
        text: "Singapore--Penang",
        href: "place/singaporepenang",
        description: "Place"
    },
    {
        text: "Malaysia--Penang",
        href: "place/malaysiapenang",
        description: "Place"
    },
    {
        text: "Minnesota--Faribault",
        href: "place/minnesotafaribault",
        description: "Place"
    },
    {
        text: "Malaysia--George Town",
        href: "place/malaysiageorge_town",
        description: "Place"
    },
    {
        text: "Mexico--Lake Pátzcuaro",
        href: "place/mexicolake_ptzcuaro",
        description: "Place"
    },
    {
        text: "Pakistan",
        href: "place/pakistan",
        description: "Place"
    },
    {
        text: "Pakistan--Pakistan--Lahore",
        href: "place/pakistanpakistanlahore",
        description: "Place"
    },
    {
        text: "Penang",
        href: "place/penang",
        description: "Place"
    },
    {
        text: "India--Lahore",
        href: "place/indialahore",
        description: "Place"
    },
    {
        text: "Earth (Planet)",
        href: "place/earth",
        description: "Place"
    },
    {
        text: "England--Dartmoor",
        href: "place/englanddartmoor",
        description: "Place"
    },
    {
        text: "Canada--Toronto",
        href: "place/canadatoronto",
        description: "Place"
    },
    {
        text: "England--Cockington",
        href: "place/englandcockington",
        description: "Place"
    },
    {
        text: "England--Wales",
        href: "place/englandwales",
        description: "Place"
    },
    {
        text: "Singapore--Tanjong Paggar",
        href: "place/singaporetanjong_paggar",
        description: "Place"
    },
    {
        text: "France--Cabourg",
        href: "place/francecabourg",
        description: "Place"
    },
    {
        text: "France--Monaco",
        href: "place/francemonaco",
        description: "Place"
    },
    {
        text: "India--Jodhpur",
        href: "place/indiajodhpur",
        description: "Place"
    },
    {
        text: "England--Burnham",
        href: "place/englandburnham",
        description: "Place"
    },
    {
        text: "Hong Kong--Victoria",
        href: "place/hong_kongvictoria",
        description: "Place"
    },
    {
        text: "Scotland--Dunbeg",
        href: "place/scotlanddunbeg",
        description: "Place"
    },
    {
        text: "Monaco--Monaco",
        href: "place/monacomonaco",
        description: "Place"
    },
    {
        text: "England--North Norfolk",
        href: "place/englandnorth_norfolk",
        description: "Place"
    },
    {
        text: "India--Bikanir (Princely State)",
        href: "place/indiabikanir",
        description: "Place"
    },
    {
        text: "India--Gwalior",
        href: "place/indiagwalior",
        description: "Place"
    },
    {
        text: "Wales--Newcastle",
        href: "place/walesnewcastle",
        description: "Place"
    },
    {
        text: "Massachusetts--Wakefield",
        href: "place/massachusettswakefield",
        description: "Place"
    },
    {
        text: "Scotland--Ettrick Bay",
        href: "place/scotlandettrick_bay",
        description: "Place"
    },
    {
        text: "Israel--Valley of Jehoshaphat",
        href: "place/israelvalley_of_jehoshaphat",
        description: "Place"
    },
    {
        text: "Scotland--Speyside Way",
        href: "place/scotlandspeyside_way",
        description: "Place"
    },
    {
        text: "California--Los Angeles--Hollywood",
        href: "place/californialos_angeleshollywood",
        description: "Place"
    },
    {
        text: "South America",
        href: "place/south_america",
        description: "Place"
    },
    {
        text: "California--Mount Wilson (Mountain)",
        href: "place/californiamount_wilson",
        description: "Place"
    },
    {
        text: "Cliveden (England)",
        href: "place/cliveden",
        description: "Place"
    },
    {
        text: "England--Addington (Croydon)",
        href: "place/englandaddington",
        description: "Place"
    },
    {
        text: "Rocky Mountains",
        href: "place/rocky_mountains",
        description: "Place"
    },
    {
        text: "England--Appledore (Torridge)",
        href: "place/englandappledore",
        description: "Place"
    },
    {
        text: "Scotland--Loch Tay",
        href: "place/scotlandloch_tay",
        description: "Place"
    },
    {
        text: "England--Chalford (Gloucestershire)",
        href: "place/englandchalford",
        description: "Place"
    },
    {
        text: "England--Chiddingstone",
        href: "place/englandchiddingstone",
        description: "Place"
    },
    {
        text: "England--Christchurch (Dorset)",
        href: "place/englandchristchurch",
        description: "Place"
    },
    {
        text: "Netherlands",
        href: "place/netherlands",
        description: "Place"
    },
    {
        text: "England--Crawley (West Sussex)",
        href: "place/englandcrawley",
        description: "Place"
    },
    {
        text: "England--Bidford-on-Avon",
        href: "place/englandbidfordonavon",
        description: "Place"
    },
    {
        text: "Palestine--Jericho",
        href: "place/palestinejericho",
        description: "Place"
    },
    {
        text: "Barbados",
        href: "place/barbados",
        description: "Place"
    },
    {
        text: "Louisiana--New Orleans",
        href: "place/louisiananew_orleans",
        description: "Place"
    },
    {
        text: "Israel--Middle East--Wilderness of Judea",
        href: "place/israelmiddle_eastwilderness_of_judea",
        description: "Place"
    },
    {
        text: "West Bank--Jericho",
        href: "place/west_bankjericho",
        description: "Place"
    },
    {
        text: "Israel--Kidron",
        href: "place/israelkidron",
        description: "Place"
    },
    {
        text: "West Bank--Hebron",
        href: "place/west_bankhebron",
        description: "Place"
    },
    {
        text: "Mexico--Popocatepetl",
        href: "place/mexicopopocatepetl",
        description: "Place"
    },
    {
        text: "India--Bengaluru",
        href: "place/indiabengaluru",
        description: "Place"
    },
    {
        text: "Nebraska",
        href: "place/nebraska",
        description: "Place"
    },
    {
        text: "England--London--Southwark",
        href: "place/englandlondonsouthwark",
        description: "Place"
    },
    {
        text: "England--Westgate-on-Sea",
        href: "place/englandwestgateonsea",
        description: "Place"
    },
    {
        text: "Iowa--Washington",
        href: "place/iowawashington",
        description: "Place"
    },
    {
        text: "England--Cliftonville",
        href: "place/englandcliftonville",
        description: "Place"
    },
    {
        text: "England--England--Newhaven",
        href: "place/englandenglandnewhaven",
        description: "Place"
    },
    {
        text: "England--Sandbach",
        href: "place/englandsandbach",
        description: "Place"
    },
    {
        text: "Mississippi--Gulfport",
        href: "place/mississippigulfport",
        description: "Place"
    },
    {
        text: "England--Cornwall",
        href: "place/englandcornwall",
        description: "Place"
    },
    {
        text: "Massachusetts--Westboro",
        href: "place/massachusettswestboro",
        description: "Place"
    },
    {
        text: "Massachusetts--Newburyport",
        href: "place/massachusettsnewburyport",
        description: "Place"
    },
    {
        text: "Massachusetts--Groton",
        href: "place/massachusettsgroton",
        description: "Place"
    },
    {
        text: "New York (State)--Frankfort",
        href: "place/new_york_statefrankfort",
        description: "Place"
    },
    {
        text: "Massachusetts--Orange",
        href: "place/massachusettsorange",
        description: "Place"
    },
    {
        text: "Iowa--Wilton",
        href: "place/iowawilton",
        description: "Place"
    },
    {
        text: "Iowa--West Union",
        href: "place/iowawest_union",
        description: "Place"
    },
    {
        text: "Iowa--Maynard",
        href: "place/iowamaynard",
        description: "Place"
    },
    {
        text: "Massachusetts--Warren",
        href: "place/massachusettswarren",
        description: "Place"
    },
    {
        text: "Massachusetts--Athol",
        href: "place/massachusettsathol",
        description: "Place"
    },
    {
        text: "Maine--Moosehead Lake",
        href: "place/mainemoosehead_lake",
        description: "Place"
    },
    {
        text: "New York (State)--New York--Ozone Park",
        href: "place/new_york_statenew_yorkozone_park",
        description: "Place"
    },
    {
        text: "Iowa--Hawarden",
        href: "place/iowahawarden",
        description: "Place"
    },
    {
        text: "Colorado--Colorado Springs",
        href: "place/coloradocolorado_springs",
        description: "Place"
    },
    {
        text: "Massachusetts--Boston--Hyde Park",
        href: "place/massachusettsbostonhyde_park",
        description: "Place"
    },
    {
        text: "Massachusetts--Florence",
        href: "place/massachusettsflorence",
        description: "Place"
    },
    {
        text: "Massachusetts--Holyoke",
        href: "place/massachusettsholyoke",
        description: "Place"
    },
    {
        text: "Maine--Boothbay Harbor",
        href: "place/maineboothbay_harbor",
        description: "Place"
    },
    {
        text: "Iowa--Brooklyn",
        href: "place/iowabrooklyn",
        description: "Place"
    },
    {
        text: "Wisconsin--Cassville",
        href: "place/wisconsincassville",
        description: "Place"
    },
    {
        text: "Iowa--Manson",
        href: "place/iowamanson",
        description: "Place"
    },
    {
        text: "Massachusetts--Norwood",
        href: "place/massachusettsnorwood",
        description: "Place"
    },
    {
        text: "Colorado--Pike's Peak",
        href: "place/coloradopikes_peak",
        description: "Place"
    },
    {
        text: "Massachusetts--Gardner",
        href: "place/massachusettsgardner",
        description: "Place"
    },
    {
        text: "Iowa--Marathon",
        href: "place/iowamarathon",
        description: "Place"
    },
    {
        text: "Massachusetts--Miller Falls",
        href: "place/massachusettsmiller_falls",
        description: "Place"
    },
    {
        text: "Wisconsin--Solon Springs",
        href: "place/wisconsinsolon_springs",
        description: "Place"
    },
    {
        text: "Wisconsin--Batavia",
        href: "place/wisconsinbatavia",
        description: "Place"
    },
    {
        text: "Massachusetts--Revere Beach",
        href: "place/massachusettsrevere_beach",
        description: "Place"
    },
    {
        text: "Wisconsin--Packwaukee",
        href: "place/wisconsinpackwaukee",
        description: "Place"
    },
    {
        text: "Massachusetts--Spencer",
        href: "place/massachusettsspencer",
        description: "Place"
    },
    {
        text: "Iowa--Springville",
        href: "place/iowaspringville",
        description: "Place"
    },
    {
        text: "India--Srinagar",
        href: "place/indiasrinagar",
        description: "Place"
    },
    {
        text: "Scotland--Melrose (Scottish Borders)",
        href: "place/scotlandmelrose",
        description: "Place"
    },
    {
        text: "Virginia--Clifton Forge",
        href: "place/virginiaclifton_forge",
        description: "Place"
    },
    {
        text: "France--Le Treport",
        href: "place/francele_treport",
        description: "Place"
    },
    {
        text: "Iowa--Ellsworth",
        href: "place/iowaellsworth",
        description: "Place"
    },
    {
        text: "Wisconsin--Hustisford",
        href: "place/wisconsinhustisford",
        description: "Place"
    },
    {
        text: "Iowa--Tabor",
        href: "place/iowatabor",
        description: "Place"
    },
    {
        text: "Pakistan--Hyderabad",
        href: "place/pakistanhyderabad",
        description: "Place"
    },
    {
        text: "England--Pitchford",
        href: "place/englandpitchford",
        description: "Place"
    },
    {
        text: "South Dakota--Badger",
        href: "place/south_dakotabadger",
        description: "Place"
    },
    {
        text: "Connecticutt--New London",
        href: "place/connecticuttnew_london",
        description: "Place"
    },
    {
        text: "Maine",
        href: "place/maine",
        description: "Place"
    },
    {
        text: "Wisconsin--Spooner",
        href: "place/wisconsinspooner",
        description: "Place"
    },
    {
        text: "New Mexico",
        href: "place/new_mexico",
        description: "Place"
    },
    {
        text: "Iowa--Wayland",
        href: "place/iowawayland",
        description: "Place"
    },
    {
        text: "Massachusetts--Duxbury Bay",
        href: "place/massachusettsduxbury_bay",
        description: "Place"
    },
    {
        text: "Iowa--Pierson",
        href: "place/iowapierson",
        description: "Place"
    },
    {
        text: "Italy--Tuscany",
        href: "place/italytuscany",
        description: "Place"
    },
    {
        text: "Czech Republic--Jiříkov",
        href: "place/czech_republicjikov",
        description: "Place"
    },
    {
        text: "Ohio--Toledo",
        href: "place/ohiotoledo",
        description: "Place"
    },
    {
        text: "Iowa--Langdon",
        href: "place/iowalangdon",
        description: "Place"
    },
    {
        text: "Wisconsin--Reedsburg",
        href: "place/wisconsinreedsburg",
        description: "Place"
    },
    {
        text: "Iowa--Coburg",
        href: "place/iowacoburg",
        description: "Place"
    },
    {
        text: "Wisconsin--Sussex",
        href: "place/wisconsinsussex",
        description: "Place"
    },
    {
        text: "Minnesota--Spring Grove",
        href: "place/minnesotaspring_grove",
        description: "Place"
    },
    {
        text: "Wisconsin--Stephensville",
        href: "place/wisconsinstephensville",
        description: "Place"
    },
    {
        text: "Iowa--Sioux City",
        href: "place/iowasioux_city",
        description: "Place"
    },
    {
        text: "Massachusetts--Ware",
        href: "place/massachusettsware",
        description: "Place"
    },
    {
        text: "Pennsylvania--Derrick City",
        href: "place/pennsylvaniaderrick_city",
        description: "Place"
    },
    {
        text: "Wisconsin--Ontario",
        href: "place/wisconsinontario",
        description: "Place"
    },
    {
        text: "Serbia--Belgrade",
        href: "place/serbiabelgrade",
        description: "Place"
    },
    {
        text: "Iowa--Grundy Center",
        href: "place/iowagrundy_center",
        description: "Place"
    },
    {
        text: "Nebraska--Battle Creek",
        href: "place/nebraskabattle_creek",
        description: "Place"
    },
    {
        text: "Wisconsin--Marshfield",
        href: "place/wisconsinmarshfield",
        description: "Place"
    },
    {
        text: "Iowa--Dysart",
        href: "place/iowadysart",
        description: "Place"
    },
    {
        text: "Wisconsin--Ripon",
        href: "place/wisconsinripon",
        description: "Place"
    },
    {
        text: "Iowa--Danbury",
        href: "place/iowadanbury",
        description: "Place"
    },
    {
        text: "Wisconsin--Osceola",
        href: "place/wisconsinosceola",
        description: "Place"
    },
    {
        text: "Iowa--West Chester",
        href: "place/iowawest_chester",
        description: "Place"
    },
    {
        text: "Iowa--Webb",
        href: "place/iowawebb",
        description: "Place"
    },
    {
        text: "Iowa--Lost Nation",
        href: "place/iowalost_nation",
        description: "Place"
    },
    {
        text: "New York (State)--Groghan",
        href: "place/new_york_stategroghan",
        description: "Place"
    },
    {
        text: "Massachusetts--Revere",
        href: "place/massachusettsrevere",
        description: "Place"
    },
    {
        text: "Maine--Biddeford",
        href: "place/mainebiddeford",
        description: "Place"
    },
    {
        text: "Iowa--Rolfe",
        href: "place/iowarolfe",
        description: "Place"
    },
    {
        text: "Massachusetts--Pepperell",
        href: "place/massachusettspepperell",
        description: "Place"
    },
    {
        text: "Massachusetts--Haverhill",
        href: "place/massachusettshaverhill",
        description: "Place"
    },
    {
        text: "Massachusetts--Natick",
        href: "place/massachusettsnatick",
        description: "Place"
    },
    {
        text: "Southern States",
        href: "place/southern_states",
        description: "Place"
    },
    {
        text: "Massachusetts--Hudson",
        href: "place/massachusettshudson",
        description: "Place"
    },
    {
        text: "New York (State)--Lake Placid",
        href: "place/new_york_statelake_placid",
        description: "Place"
    },
    {
        text: "Massachusetts--South Athol",
        href: "place/massachusettssouth_athol",
        description: "Place"
    },
    {
        text: "Iowa--Fostoria",
        href: "place/iowafostoria",
        description: "Place"
    },
    {
        text: "Singapore--Rochore River",
        href: "place/singaporerochore_river",
        description: "Place"
    },
    {
        text: "Massachusetts--Townsend",
        href: "place/massachusettstownsend",
        description: "Place"
    },
    {
        text: "Massachusetts--Georgetown",
        href: "place/massachusettsgeorgetown",
        description: "Place"
    },
    {
        text: "Iowa--Dows",
        href: "place/iowadows",
        description: "Place"
    },
    {
        text: "Iowa--Lake Park",
        href: "place/iowalake_park",
        description: "Place"
    },
    {
        text: "Massachusetts--Winthrop Centre",
        href: "place/massachusettswinthrop_centre",
        description: "Place"
    },
    {
        text: "Iowa--Le Mars",
        href: "place/iowale_mars",
        description: "Place"
    },
    {
        text: "Massachusetts--Reading",
        href: "place/massachusettsreading",
        description: "Place"
    },
    {
        text: "Massachusetts--Charles River",
        href: "place/massachusettscharles_river",
        description: "Place"
    },
    {
        text: "Massachusetts--Salisbury Beach",
        href: "place/massachusettssalisbury_beach",
        description: "Place"
    },
    {
        text: "Maine--Shapleigh",
        href: "place/maineshapleigh",
        description: "Place"
    },
    {
        text: "Maine--Kennebunk",
        href: "place/mainekennebunk",
        description: "Place"
    },
    {
        text: "Massachusetts--Stow",
        href: "place/massachusettsstow",
        description: "Place"
    },
    {
        text: "Maine--Alfred",
        href: "place/mainealfred",
        description: "Place"
    },
    {
        text: "England--Wanstead",
        href: "place/englandwanstead",
        description: "Place"
    },
    {
        text: "Massachusetts--West Newbury",
        href: "place/massachusettswest_newbury",
        description: "Place"
    },
    {
        text: "Massachusetts--Lancaster",
        href: "place/massachusettslancaster",
        description: "Place"
    },
    {
        text: "Massachusetts--Marlboro",
        href: "place/massachusettsmarlboro",
        description: "Place"
    },
    {
        text: "Iowa--Lester",
        href: "place/iowalester",
        description: "Place"
    },
    {
        text: "Massachusetts--Clinton",
        href: "place/massachusettsclinton",
        description: "Place"
    },
    {
        text: "Massachusetts--Milford",
        href: "place/massachusettsmilford",
        description: "Place"
    },
    {
        text: "Massachusetts--Grafton",
        href: "place/massachusettsgrafton",
        description: "Place"
    },
    {
        text: "Massachusetts--South Acton",
        href: "place/massachusettssouth_acton",
        description: "Place"
    },
    {
        text: "Maine--Cape Porpoise",
        href: "place/mainecape_porpoise",
        description: "Place"
    },
    {
        text: "New York (State)--Ticonderoga",
        href: "place/new_york_stateticonderoga",
        description: "Place"
    },
    {
        text: "Massachusetts--Baldwinville",
        href: "place/massachusettsbaldwinville",
        description: "Place"
    },
    {
        text: "Kansas--Fort Scott",
        href: "place/kansasfort_scott",
        description: "Place"
    },
    {
        text: "Iowa--Whitten",
        href: "place/iowawhitten",
        description: "Place"
    },
    {
        text: "New York (State)--Rouses Point",
        href: "place/new_york_staterouses_point",
        description: "Place"
    },
    {
        text: "Iowa--Newton",
        href: "place/iowanewton",
        description: "Place"
    },
    {
        text: "Massachusetts--Turners Falls",
        href: "place/massachusettsturners_falls",
        description: "Place"
    },
    {
        text: "Virginia--Staunton",
        href: "place/virginiastaunton",
        description: "Place"
    },
    {
        text: "Iowa--Parkersburg",
        href: "place/iowaparkersburg",
        description: "Place"
    },
    {
        text: "Minnesota--Wadena",
        href: "place/minnesotawadena",
        description: "Place"
    },
    {
        text: "Wisconsin--Cobb",
        href: "place/wisconsincobb",
        description: "Place"
    },
    {
        text: "South Dakota--Miller",
        href: "place/south_dakotamiller",
        description: "Place"
    },
    {
        text: "Iowa--Gladbrook",
        href: "place/iowagladbrook",
        description: "Place"
    },
    {
        text: "Iowa--Rose Hill",
        href: "place/iowarose_hill",
        description: "Place"
    },
    {
        text: "Wisconsin--Portage",
        href: "place/wisconsinportage",
        description: "Place"
    },
    {
        text: "Wisconsin--Hika",
        href: "place/wisconsinhika",
        description: "Place"
    },
    {
        text: "New York (State)--Whitehall",
        href: "place/new_york_statewhitehall",
        description: "Place"
    },
    {
        text: "New York (State)--Oakfield",
        href: "place/new_york_stateoakfield",
        description: "Place"
    },
    {
        text: "Massachusetts--Melrose",
        href: "place/massachusettsmelrose",
        description: "Place"
    },
    {
        text: "Arizona--Phoenix",
        href: "place/arizonaphoenix",
        description: "Place"
    },
    {
        text: "Maine--York Beach",
        href: "place/maineyork_beach",
        description: "Place"
    },
    {
        text: "Maine--Webhannet Beach",
        href: "place/mainewebhannet_beach",
        description: "Place"
    },
    {
        text: "Maine--Ogunquit",
        href: "place/maineogunquit",
        description: "Place"
    },
    {
        text: "Maine--Kennebunkport",
        href: "place/mainekennebunkport",
        description: "Place"
    },
    {
        text: "Texas--Hillsboro",
        href: "place/texashillsboro",
        description: "Place"
    },
    {
        text: "New York (State)--Highland",
        href: "place/new_york_statehighland",
        description: "Place"
    },
    {
        text: "Iowa--Victor",
        href: "place/iowavictor",
        description: "Place"
    },
    {
        text: "Massachusetts--Greenfield",
        href: "place/massachusettsgreenfield",
        description: "Place"
    },
    {
        text: "New York (State)--New York--Woodhaven",
        href: "place/new_york_statenew_yorkwoodhaven",
        description: "Place"
    },
    {
        text: "Illinois--Aurora",
        href: "place/illinoisaurora",
        description: "Place"
    },
    {
        text: "England--Burnley",
        href: "place/englandburnley",
        description: "Place"
    },
    {
        text: "New York (State)--Warwick",
        href: "place/new_york_statewarwick",
        description: "Place"
    },
    {
        text: "Indiana--Marion",
        href: "place/indianamarion",
        description: "Place"
    },
    {
        text: "Ohio--Dayton",
        href: "place/ohiodayton",
        description: "Place"
    },
    {
        text: "England--Irstead",
        href: "place/englandirstead",
        description: "Place"
    },
    {
        text: "Brazil--Olinda",
        href: "place/brazilolinda",
        description: "Place"
    },
    {
        text: "England--Wight",
        href: "place/englandwight",
        description: "Place"
    },
    {
        text: "France--Laruns",
        href: "place/francelaruns",
        description: "Place"
    },
    {
        text: "Wales--Caerphilly",
        href: "place/walescaerphilly",
        description: "Place"
    },
    {
        text: "England--Melrose",
        href: "place/englandmelrose",
        description: "Place"
    },
    {
        text: "France--Eaux-Chaudes",
        href: "place/franceeauxchaudes",
        description: "Place"
    },
    {
        text: "England--Appledore (Kent)",
        href: "place/englandappledore",
        description: "Place"
    },
    {
        text: "England--Beaconsfield",
        href: "place/englandbeaconsfield",
        description: "Place"
    },
    {
        text: "Scotland--Kelso",
        href: "place/scotlandkelso",
        description: "Place"
    },
    {
        text: "India--Rajputana",
        href: "place/indiarajputana",
        description: "Place"
    },
    {
        text: "England--Pulborough",
        href: "place/englandpulborough",
        description: "Place"
    },
    {
        text: "England--Beccles",
        href: "place/englandbeccles",
        description: "Place"
    },
    {
        text: "France--Treport",
        href: "place/francetreport",
        description: "Place"
    },
    {
        text: "England--Edwinstowe",
        href: "place/englandedwinstowe",
        description: "Place"
    },
    {
        text: "England--Sherwood Forest",
        href: "place/englandsherwood_forest",
        description: "Place"
    },
    {
        text: "Italy--Veneto",
        href: "place/italyveneto",
        description: "Place"
    },
    {
        text: "Scotland--West Dunbartonshire",
        href: "place/scotlandwest_dunbartonshire",
        description: "Place"
    },
    {
        text: "Wales--Porthkerry",
        href: "place/walesporthkerry",
        description: "Place"
    },
    {
        text: "England--Kinver",
        href: "place/englandkinver",
        description: "Place"
    },
    {
        text: "England--Bassenthwaite Lake",
        href: "place/englandbassenthwaite_lake",
        description: "Place"
    },
    {
        text: "Scotland--Loch Maree",
        href: "place/scotlandloch_maree",
        description: "Place"
    },
    {
        text: "Scotland--Matlock",
        href: "place/scotlandmatlock",
        description: "Place"
    },
    {
        text: "Scotland--Beauly",
        href: "place/scotlandbeauly",
        description: "Place"
    },
    {
        text: "England--Kennack Bay",
        href: "place/englandkennack_bay",
        description: "Place"
    },
    {
        text: "England--Sutton (Outer borough of London)",
        href: "place/englandsutton",
        description: "Place"
    },
    {
        text: "England--Northbourne",
        href: "place/englandnorthbourne",
        description: "Place"
    },
    {
        text: "England--Penshurst",
        href: "place/englandpenshurst",
        description: "Place"
    },
    {
        text: "New Zealand--Lyttleton",
        href: "place/new_zealandlyttleton",
        description: "Place"
    },
    {
        text: "Australia--Australian Alps",
        href: "place/australiaaustralian_alps",
        description: "Place"
    },
    {
        text: "Italy--Napoli",
        href: "place/italynapoli",
        description: "Place"
    },
    {
        text: "England--Chidingstone",
        href: "place/englandchidingstone",
        description: "Place"
    },
    {
        text: "Italy--Ravello",
        href: "place/italyravello",
        description: "Place"
    },
    {
        text: "Italy--Cetara",
        href: "place/italycetara",
        description: "Place"
    },
    {
        text: "Italy--Ischia",
        href: "place/italyischia",
        description: "Place"
    },
    {
        text: "Japan--Nikko",
        href: "place/japannikko",
        description: "Place"
    },
    {
        text: "England--Howtown",
        href: "place/englandhowtown",
        description: "Place"
    },
    {
        text: "Wales--Colwyn",
        href: "place/walescolwyn",
        description: "Place"
    },
    {
        text: "England--Chalfont St. Giles",
        href: "place/englandchalfont_st_giles",
        description: "Place"
    },
    {
        text: "England--Medway",
        href: "place/englandmedway",
        description: "Place"
    },
    {
        text: "England--Writtle",
        href: "place/englandwrittle",
        description: "Place"
    },
    {
        text: "England--Saffron Walden",
        href: "place/englandsaffron_walden",
        description: "Place"
    },
    {
        text: "England--North Devon",
        href: "place/englandnorth_devon",
        description: "Place"
    },
    {
        text: "England--Tuckenhay",
        href: "place/englandtuckenhay",
        description: "Place"
    },
    {
        text: "England--Avon",
        href: "place/englandavon",
        description: "Place"
    },
    {
        text: "England--North Berwick",
        href: "place/englandnorth_berwick",
        description: "Place"
    },
    {
        text: "Puerto Rico--Aguadilla",
        href: "place/puerto_ricoaguadilla",
        description: "Place"
    },
    {
        text: "Scotland--Dunbartonshire",
        href: "place/scotlanddunbartonshire",
        description: "Place"
    },
    {
        text: "Scotland--Loch Gare",
        href: "place/scotlandloch_gare",
        description: "Place"
    },
    {
        text: "Italy--Bay of Naples Region",
        href: "place/italybay_of_naples_region",
        description: "Place"
    },
    {
        text: "England--Jenkin Swatchway",
        href: "place/englandjenkin_swatchway",
        description: "Place"
    },
    {
        text: "England--Shoeburyness",
        href: "place/englandshoeburyness",
        description: "Place"
    },
    {
        text: "England--Steyning",
        href: "place/englandsteyning",
        description: "Place"
    },
    {
        text: "Scotland--Whistlefield",
        href: "place/scotlandwhistlefield",
        description: "Place"
    },
    {
        text: "Poland--Szczecin",
        href: "place/polandszczecin",
        description: "Place"
    },
    {
        text: "Italy--Arcole",
        href: "place/italyarcole",
        description: "Place"
    },
    {
        text: "Wales--Newport",
        href: "place/walesnewport",
        description: "Place"
    },
    {
        text: "England--Sandford (Devon)",
        href: "place/englandsandford",
        description: "Place"
    },
    {
        text: "England--Hertford",
        href: "place/englandhertford",
        description: "Place"
    },
    {
        text: "England--Leigh-on-Sea",
        href: "place/englandleighonsea",
        description: "Place"
    },
    {
        text: "Wales--Llyfnant Valley",
        href: "place/walesllyfnant_valley",
        description: "Place"
    },
    {
        text: "Morocco--Essaouira",
        href: "place/moroccoessaouira",
        description: "Place"
    },
    {
        text: "Wales--Aberdovey",
        href: "place/walesaberdovey",
        description: "Place"
    },
    {
        text: "Morocco--Mazagan",
        href: "place/moroccomazagan",
        description: "Place"
    },
    {
        text: "Morocco--Tangiers",
        href: "place/moroccotangiers",
        description: "Place"
    },
    {
        text: "England--Newton Abbot",
        href: "place/englandnewton_abbot",
        description: "Place"
    },
    {
        text: "England--Pebworth",
        href: "place/englandpebworth",
        description: "Place"
    },
    {
        text: "England--Broad Marston",
        href: "place/englandbroad_marston",
        description: "Place"
    },
    {
        text: "Great Britain Miscellaneous Island Dependencies--Castletown (Isle of Man)",
        href: "place/great_britain_miscellaneous_island_dependenciescastletown",
        description: "Place"
    },
    {
        text: "Trinidad and Tobago",
        href: "place/trinidad_and_tobago",
        description: "Place"
    },
    {
        text: "England--Burton upon Trent",
        href: "place/englandburton_upon_trent",
        description: "Place"
    },
    {
        text: "Wales--Newborough",
        href: "place/walesnewborough",
        description: "Place"
    },
    {
        text: "Japan--Nagasaki-shi",
        href: "place/japannagasakishi",
        description: "Place"
    },
    {
        text: "Tunisia--Korbus",
        href: "place/tunisiakorbus",
        description: "Place"
    },
    {
        text: "Monaco",
        href: "place/monaco",
        description: "Place"
    },
    {
        text: "Scotland--Adrishaig",
        href: "place/scotlandadrishaig",
        description: "Place"
    },
    {
        text: "Scotland--Glen lyon",
        href: "place/scotlandglen_lyon",
        description: "Place"
    },
    {
        text: "England--Bowness-on-Solway",
        href: "place/englandbownessonsolway",
        description: "Place"
    },
    {
        text: "Wales--Bettws Newydd",
        href: "place/walesbettws_newydd",
        description: "Place"
    },
    {
        text: "England--Douglas",
        href: "place/englanddouglas",
        description: "Place"
    },
    {
        text: "England--Tintagel",
        href: "place/englandtintagel",
        description: "Place"
    },
    {
        text: "Wales--Cricceth",
        href: "place/walescricceth",
        description: "Place"
    },
    {
        text: "Algeria--Algiers",
        href: "place/algeriaalgiers",
        description: "Place"
    },
    {
        text: "England--Rowsley",
        href: "place/englandrowsley",
        description: "Place"
    },
    {
        text: "Scotland--St. Monans",
        href: "place/scotlandst_monans",
        description: "Place"
    },
    {
        text: "Scotland--Crieff",
        href: "place/scotlandcrieff",
        description: "Place"
    },
    {
        text: "Connecticut--Ridgefield",
        href: "place/connecticutridgefield",
        description: "Place"
    },
    {
        text: "New Jersey--Hackettstown",
        href: "place/new_jerseyhackettstown",
        description: "Place"
    },
    {
        text: "Washington (State)",
        href: "place/washington",
        description: "Place"
    },
    {
        text: "Scotland--Largs",
        href: "place/scotlandlargs",
        description: "Place"
    },
    {
        text: "England--Cleethorpes",
        href: "place/englandcleethorpes",
        description: "Place"
    },
    {
        text: "Scotland--Kinross-shire",
        href: "place/scotlandkinrossshire",
        description: "Place"
    },
    {
        text: "Scotland--Scotland--Trossachs",
        href: "place/scotlandscotlandtrossachs",
        description: "Place"
    },
    {
        text: "England--Hally",
        href: "place/englandhally",
        description: "Place"
    },
    {
        text: "Scotland--West Highlands",
        href: "place/scotlandwest_highlands",
        description: "Place"
    },
    {
        text: "England--Hurley (Windsor and Maidenhead)",
        href: "place/englandhurley",
        description: "Place"
    },
    {
        text: "Wales--Llandaff",
        href: "place/walesllandaff",
        description: "Place"
    },
    {
        text: "Scotland--Largo",
        href: "place/scotlandlargo",
        description: "Place"
    },
    {
        text: "Scotland--Cullen",
        href: "place/scotlandcullen",
        description: "Place"
    },
    {
        text: "England--Blackburn",
        href: "place/englandblackburn",
        description: "Place"
    },
    {
        text: "England--Halton",
        href: "place/englandhalton",
        description: "Place"
    },
    {
        text: "Norway--Oppland fylke",
        href: "place/norwayoppland_fylke",
        description: "Place"
    },
    {
        text: "Wales--Caernarvonshire",
        href: "place/walescaernarvonshire",
        description: "Place"
    },
    {
        text: "England--Totland",
        href: "place/englandtotland",
        description: "Place"
    },
    {
        text: "Scotland--Forres",
        href: "place/scotlandforres",
        description: "Place"
    },
    {
        text: "England--Calbourne",
        href: "place/englandcalbourne",
        description: "Place"
    },
    {
        text: "England--Sopley",
        href: "place/englandsopley",
        description: "Place"
    },
    {
        text: "England--Morton",
        href: "place/englandmorton",
        description: "Place"
    },
    {
        text: "Wales--Haverfordwest",
        href: "place/waleshaverfordwest",
        description: "Place"
    },
    {
        text: "Washington (State)--Spokane",
        href: "place/washington_statespokane",
        description: "Place"
    },
    {
        text: "Scotland--Carlisle",
        href: "place/scotlandcarlisle",
        description: "Place"
    },
    {
        text: "New Jersey--Red Bank",
        href: "place/new_jerseyred_bank",
        description: "Place"
    },
    {
        text: "New Jersey--Trenton",
        href: "place/new_jerseytrenton",
        description: "Place"
    },
    {
        text: "Thailand",
        href: "place/thailand",
        description: "Place"
    },
    {
        text: "Colorado--Central City",
        href: "place/coloradocentral_city",
        description: "Place"
    },
    {
        text: "England--Clifton",
        href: "place/englandclifton",
        description: "Place"
    },
    {
        text: "Iraq--Baghdad",
        href: "place/iraqbaghdad",
        description: "Place"
    },
    {
        text: "Turkey",
        href: "place/turkey",
        description: "Place"
    },
    {
        text: "Israel--Lake Tiberias",
        href: "place/israellake_tiberias",
        description: "Place"
    },
    {
        text: "Scotland--Row Village",
        href: "place/scotlandrow_village",
        description: "Place"
    },
    {
        text: "Wales--Pontyclun",
        href: "place/walespontyclun",
        description: "Place"
    },
    {
        text: "England--Borrowdale (Valley)",
        href: "place/englandborrowdale",
        description: "Place"
    },
    {
        text: "Wales--Holyhead",
        href: "place/walesholyhead",
        description: "Place"
    },
    {
        text: "Wales--Llanfair Pwllgwyngyll",
        href: "place/walesllanfair_pwllgwyngyll",
        description: "Place"
    },
    {
        text: "England--Lyndhurst",
        href: "place/englandlyndhurst",
        description: "Place"
    },
    {
        text: "England--Stockport",
        href: "place/englandstockport",
        description: "Place"
    },
    {
        text: "England--Pevensey",
        href: "place/englandpevensey",
        description: "Place"
    },
    {
        text: "Middle East",
        href: "place/middle_east",
        description: "Place"
    },
    {
        text: "France--Lisieux",
        href: "place/francelisieux",
        description: "Place"
    },
    {
        text: "Massachusetts",
        href: "place/massachusetts",
        description: "Place"
    },
    {
        text: "Utah--Salt Lake City",
        href: "place/utahsalt_lake_city",
        description: "Place"
    },
    {
        text: "New York (State)--Haverstraw",
        href: "place/new_york_statehaverstraw",
        description: "Place"
    },
    {
        text: "England--Hawkshead",
        href: "place/englandhawkshead",
        description: "Place"
    },
    {
        text: "Connecticut--Norwalk",
        href: "place/connecticutnorwalk",
        description: "Place"
    },
    {
        text: "Puerto Rico--Dorado",
        href: "place/puerto_ricodorado",
        description: "Place"
    },
    {
        text: "New York (State)--Highland Falls",
        href: "place/new_york_statehighland_falls",
        description: "Place"
    },
    {
        text: "England--Midhurst",
        href: "place/englandmidhurst",
        description: "Place"
    },
    {
        text: "England--Newchurch",
        href: "place/englandnewchurch",
        description: "Place"
    },
    {
        text: "United States Virgin Islands",
        href: "place/united_states_virgin_islands",
        description: "Place"
    },
    {
        text: "England--Lundy Island",
        href: "place/englandlundy_island",
        description: "Place"
    },
    {
        text: "Scotland--Muchalls",
        href: "place/scotlandmuchalls",
        description: "Place"
    },
    {
        text: "Scotland--Fair Isle",
        href: "place/scotlandfair_isle",
        description: "Place"
    },
    {
        text: "Scotland--Killarney",
        href: "place/scotlandkillarney",
        description: "Place"
    },
    {
        text: "England--Newby",
        href: "place/englandnewby",
        description: "Place"
    },
    {
        text: "Florida--Saint Augustine",
        href: "place/floridasaint_augustine",
        description: "Place"
    },
    {
        text: "Minnesota--Moorhead",
        href: "place/minnesotamoorhead",
        description: "Place"
    },
    {
        text: "Washington (State)--Seattle",
        href: "place/washington_stateseattle",
        description: "Place"
    },
    {
        text: "England--Wasdale",
        href: "place/englandwasdale",
        description: "Place"
    },
    {
        text: "Scotland--Innellan",
        href: "place/scotlandinnellan",
        description: "Place"
    },
    {
        text: "Wales--Harlech",
        href: "place/walesharlech",
        description: "Place"
    },
    {
        text: "England--Puddletown",
        href: "place/englandpuddletown",
        description: "Place"
    },
    {
        text: "England--Gorleston-on-Sea",
        href: "place/englandgorlestononsea",
        description: "Place"
    },
    {
        text: "England--Shepton Mallet",
        href: "place/englandshepton_mallet",
        description: "Place"
    },
    {
        text: "England--Cheddar",
        href: "place/englandcheddar",
        description: "Place"
    },
    {
        text: "England--Scalby Mills",
        href: "place/englandscalby_mills",
        description: "Place"
    },
    {
        text: "Ghana--Accra",
        href: "place/ghanaaccra",
        description: "Place"
    },
    {
        text: "Wales--Brecknock",
        href: "place/walesbrecknock",
        description: "Place"
    },
    {
        text: "West Bank--Bethany",
        href: "place/west_bankbethany",
        description: "Place"
    },
    {
        text: "Scotland--Ballinluig",
        href: "place/scotlandballinluig",
        description: "Place"
    },
    {
        text: "Wales--Llanvihangel Ystern Llewern",
        href: "place/walesllanvihangel_ystern_llewern",
        description: "Place"
    },
    {
        text: "England--Ivybridge",
        href: "place/englandivybridge",
        description: "Place"
    },
    {
        text: "Scotland--Roslin",
        href: "place/scotlandroslin",
        description: "Place"
    },
    {
        text: "England--Southbourne",
        href: "place/englandsouthbourne",
        description: "Place"
    },
    {
        text: "England--Llanberis",
        href: "place/englandllanberis",
        description: "Place"
    },
    {
        text: "Scotland--Tay River Valley",
        href: "place/scotlandtay_river_valley",
        description: "Place"
    },
    {
        text: "California--Porterville",
        href: "place/californiaporterville",
        description: "Place"
    },
    {
        text: "New York (State)--Huntington",
        href: "place/new_york_statehuntington",
        description: "Place"
    },
    {
        text: "England--Endon",
        href: "place/englandendon",
        description: "Place"
    },
    {
        text: "England--Taplow",
        href: "place/englandtaplow",
        description: "Place"
    },
    {
        text: "England--Shiplake",
        href: "place/englandshiplake",
        description: "Place"
    },
    {
        text: "Vatican City",
        href: "place/vatican_city",
        description: "Place"
    },
    {
        text: "Puerto Rico--Caguas",
        href: "place/puerto_ricocaguas",
        description: "Place"
    },
    {
        text: "England--Itchington",
        href: "place/englanditchington",
        description: "Place"
    },
    {
        text: "England--Berkswell",
        href: "place/englandberkswell",
        description: "Place"
    },
    {
        text: "England--Dunchurch",
        href: "place/englanddunchurch",
        description: "Place"
    },
    {
        text: "England--Little Wolford",
        href: "place/englandlittle_wolford",
        description: "Place"
    },
    {
        text: "New Jersey--West End",
        href: "place/new_jerseywest_end",
        description: "Place"
    },
    {
        text: "Georgia--Pine Mountain",
        href: "place/georgiapine_mountain",
        description: "Place"
    },
    {
        text: "Wales--Cardigan",
        href: "place/walescardigan",
        description: "Place"
    },
    {
        text: "Norway--Balestrand",
        href: "place/norwaybalestrand",
        description: "Place"
    },
    {
        text: "Norway--Molde",
        href: "place/norwaymolde",
        description: "Place"
    },
    {
        text: "Italy--Viareggio",
        href: "place/italyviareggio",
        description: "Place"
    },
    {
        text: "England--Efford",
        href: "place/englandefford",
        description: "Place"
    },
    {
        text: "France--Goust",
        href: "place/francegoust",
        description: "Place"
    },
    {
        text: "Alaska",
        href: "place/alaska",
        description: "Place"
    },
    {
        text: "Scotland--Portknockie",
        href: "place/scotlandportknockie",
        description: "Place"
    },
    {
        text: "Scotland--Deeside",
        href: "place/scotlanddeeside",
        description: "Place"
    },
    {
        text: "Wales--Bettws Penpont",
        href: "place/walesbettws_penpont",
        description: "Place"
    },
    {
        text: "Croatia--Split",
        href: "place/croatiasplit",
        description: "Place"
    },
    {
        text: "England--Cricklade",
        href: "place/englandcricklade",
        description: "Place"
    },
    {
        text: "England--England--Norfolk",
        href: "place/englandenglandnorfolk",
        description: "Place"
    },
    {
        text: "Scotland--Glen Etive",
        href: "place/scotlandglen_etive",
        description: "Place"
    },
    {
        text: "England--Gibraltar",
        href: "place/englandgibraltar",
        description: "Place"
    },
    {
        text: "England--Rydal",
        href: "place/englandrydal",
        description: "Place"
    },
    {
        text: "England--Borrowdale",
        href: "place/englandborrowdale",
        description: "Place"
    },
    {
        text: "England--Kensington",
        href: "place/englandkensington",
        description: "Place"
    },
    {
        text: "Antarctica--South Pole",
        href: "place/antarcticasouth_pole",
        description: "Place"
    },
    {
        text: "Scotland--Loch Arklet",
        href: "place/scotlandloch_arklet",
        description: "Place"
    },
    {
        text: "India--Shipbur",
        href: "place/indiashipbur",
        description: "Place"
    },
    {
        text: "India--Calcutta",
        href: "place/indiacalcutta",
        description: "Place"
    },
    {
        text: "Asia",
        href: "place/asia",
        description: "Place"
    },
    {
        text: "England--Brading",
        href: "place/englandbrading",
        description: "Place"
    },
    {
        text: "England--Great Langdale",
        href: "place/englandgreat_langdale",
        description: "Place"
    },
    {
        text: "England--Isle of Man",
        href: "place/englandisle_of_man",
        description: "Place"
    },
    {
        text: "England--Molesey",
        href: "place/englandmolesey",
        description: "Place"
    },
    {
        text: "Japan--Kyoto",
        href: "place/japankyoto",
        description: "Place"
    },
    {
        text: "Scotland--Inverfairigaig",
        href: "place/scotlandinverfairigaig",
        description: "Place"
    },
    {
        text: "England--Eton",
        href: "place/englandeton",
        description: "Place"
    },
    {
        text: "Scotland--Lynton",
        href: "place/scotlandlynton",
        description: "Place"
    },
    {
        text: "Wales--Carnarvon",
        href: "place/walescarnarvon",
        description: "Place"
    },
    {
        text: "France--Le Mont-Saint-Michel",
        href: "place/francele_montsaintmichel",
        description: "Place"
    },
    {
        text: "England--Saltash",
        href: "place/englandsaltash",
        description: "Place"
    },
    {
        text: "France--Dinard",
        href: "place/francedinard",
        description: "Place"
    },
    {
        text: "Argentina",
        href: "place/argentina",
        description: "Place"
    },
    {
        text: "Argentina--Puente del Inca",
        href: "place/argentinapuente_del_inca",
        description: "Place"
    },
    {
        text: "Poland--Sopot",
        href: "place/polandsopot",
        description: "Place"
    },
    {
        text: "Poland--Danzig",
        href: "place/polanddanzig",
        description: "Place"
    },
    {
        text: "England--Ripley",
        href: "place/englandripley",
        description: "Place"
    },
    {
        text: "England--Perthshire",
        href: "place/englandperthshire",
        description: "Place"
    },
    {
        text: "France--Nurnberg",
        href: "place/francenurnberg",
        description: "Place"
    },
    {
        text: "Wales--Cader Idris",
        href: "place/walescader_idris",
        description: "Place"
    },
    {
        text: "Italy--Riva",
        href: "place/italyriva",
        description: "Place"
    },
    {
        text: "England--Wraysbury",
        href: "place/englandwraysbury",
        description: "Place"
    },
    {
        text: "Sri Lanka--Nuwara Eliya",
        href: "place/sri_lankanuwara_eliya",
        description: "Place"
    },
    {
        text: "England--Belaugh",
        href: "place/englandbelaugh",
        description: "Place"
    },
    {
        text: "New Jersey--Lakewood",
        href: "place/new_jerseylakewood",
        description: "Place"
    },
    {
        text: "Fiji--Vanua Lev",
        href: "place/fijivanua_lev",
        description: "Place"
    },
    {
        text: "Wales--River Mawddach",
        href: "place/walesriver_mawddach",
        description: "Place"
    },
    {
        text: "England--Tissington",
        href: "place/englandtissington",
        description: "Place"
    },
    {
        text: "England--Sandown",
        href: "place/englandsandown",
        description: "Place"
    },
    {
        text: "Connecticut--Wilton",
        href: "place/connecticutwilton",
        description: "Place"
    },
    {
        text: "Wisconsin--Wisconsin Dells",
        href: "place/wisconsinwisconsin_dells",
        description: "Place"
    },
    {
        text: "England--Dorchester (Oxfordshire)",
        href: "place/englanddorchester",
        description: "Place"
    },
    {
        text: "England--Whitchurch",
        href: "place/englandwhitchurch",
        description: "Place"
    },
    {
        text: "England--Woodstock",
        href: "place/englandwoodstock",
        description: "Place"
    },
    {
        text: "England--Minster Lovell",
        href: "place/englandminster_lovell",
        description: "Place"
    },
    {
        text: "England--Witney",
        href: "place/englandwitney",
        description: "Place"
    },
    {
        text: "Missouri",
        href: "place/missouri",
        description: "Place"
    },
    {
        text: "Wisconsin--Waukesha",
        href: "place/wisconsinwaukesha",
        description: "Place"
    },
    {
        text: "Scotland--Ben Liughach",
        href: "place/scotlandben_liughach",
        description: "Place"
    },
    {
        text: "England--River Don (South Yorkshire)",
        href: "place/englandriver_don",
        description: "Place"
    },
    {
        text: "New Jersey--Park Ridge",
        href: "place/new_jerseypark_ridge",
        description: "Place"
    },
    {
        text: "Scotland--Ardrishaig",
        href: "place/scotlandardrishaig",
        description: "Place"
    },
    {
        text: "England--West Runton",
        href: "place/englandwest_runton",
        description: "Place"
    },
    {
        text: "England--Runton",
        href: "place/englandrunton",
        description: "Place"
    },
    {
        text: "France--Jersey",
        href: "place/francejersey",
        description: "Place"
    },
    {
        text: "England--Staithes Beck",
        href: "place/englandstaithes_beck",
        description: "Place"
    },
    {
        text: "England--Kildale",
        href: "place/englandkildale",
        description: "Place"
    },
    {
        text: "England--Guisborough",
        href: "place/englandguisborough",
        description: "Place"
    },
    {
        text: "England--Minstead",
        href: "place/englandminstead",
        description: "Place"
    },
    {
        text: "Wales--Alteryn",
        href: "place/walesalteryn",
        description: "Place"
    },
    {
        text: "New York (State)--Northport",
        href: "place/new_york_statenorthport",
        description: "Place"
    },
    {
        text: "Scotland--Jedburgh",
        href: "place/scotlandjedburgh",
        description: "Place"
    },
    {
        text: "Florida--Hillsboro Beach",
        href: "place/floridahillsboro_beach",
        description: "Place"
    },
    {
        text: "Scotland--Scottish Borders",
        href: "place/scotlandscottish_borders",
        description: "Place"
    },
    {
        text: "New York (State)--Rockville Centre",
        href: "place/new_york_staterockville_centre",
        description: "Place"
    },
    {
        text: "New York (State)--Baldwin",
        href: "place/new_york_statebaldwin",
        description: "Place"
    },
    {
        text: "France--Bidarray",
        href: "place/francebidarray",
        description: "Place"
    },
    {
        text: "England--Wroxton",
        href: "place/englandwroxton",
        description: "Place"
    },
    {
        text: "England--Princetown",
        href: "place/englandprincetown",
        description: "Place"
    },
    {
        text: "England--Millbrook",
        href: "place/englandmillbrook",
        description: "Place"
    },
    {
        text: "West Virginia--Parkersburg",
        href: "place/west_virginiaparkersburg",
        description: "Place"
    },
    {
        text: "Scotland--Kyltra",
        href: "place/scotlandkyltra",
        description: "Place"
    },
    {
        text: "Wales--Banavie",
        href: "place/walesbanavie",
        description: "Place"
    },
    {
        text: "Scotland--Glenfinnan",
        href: "place/scotlandglenfinnan",
        description: "Place"
    },
    {
        text: "Scotland--Corpach",
        href: "place/scotlandcorpach",
        description: "Place"
    },
    {
        text: "Kentucky--Mount Sterling",
        href: "place/kentuckymount_sterling",
        description: "Place"
    },
    {
        text: "England--Bridport",
        href: "place/englandbridport",
        description: "Place"
    },
    {
        text: "England--East Stoke",
        href: "place/englandeast_stoke",
        description: "Place"
    },
    {
        text: "England--Lulworth",
        href: "place/englandlulworth",
        description: "Place"
    },
    {
        text: "Scotland--Annan Water",
        href: "place/scotlandannan_water",
        description: "Place"
    },
    {
        text: "Scotland--Achray Forest",
        href: "place/scotlandachray_forest",
        description: "Place"
    },
    {
        text: "Scotland--Glen Finglas",
        href: "place/scotlandglen_finglas",
        description: "Place"
    },
    {
        text: "Morocco--Fez",
        href: "place/moroccofez",
        description: "Place"
    },
    {
        text: "Morocco--Mequinez",
        href: "place/moroccomequinez",
        description: "Place"
    },
    {
        text: "Morocco--Larache",
        href: "place/moroccolarache",
        description: "Place"
    },
    {
        text: "New Jersey--Deal Lake",
        href: "place/new_jerseydeal_lake",
        description: "Place"
    },
    {
        text: "Portugal--Sintra",
        href: "place/portugalsintra",
        description: "Place"
    },
    {
        text: "Scotland--Oban Region",
        href: "place/scotlandoban_region",
        description: "Place"
    },
    {
        text: "Manitoba",
        href: "place/manitoba",
        description: "Place"
    },
    {
        text: "England--Tintern",
        href: "place/englandtintern",
        description: "Place"
    },
    {
        text: "England--Ross-on-Wye",
        href: "place/englandrossonwye",
        description: "Place"
    },
    {
        text: "Delaware",
        href: "place/delaware",
        description: "Place"
    },
    {
        text: "England--Stroud",
        href: "place/englandstroud",
        description: "Place"
    },
    {
        text: "Scotland--Onich",
        href: "place/scotlandonich",
        description: "Place"
    },
    {
        text: "Scotland--Fochabers",
        href: "place/scotlandfochabers",
        description: "Place"
    },
    {
        text: "Iowa",
        href: "place/iowa",
        description: "Place"
    },
    {
        text: "Mexico--Acapulco",
        href: "place/mexicoacapulco",
        description: "Place"
    },
    {
        text: "Scotland--Jura",
        href: "place/scotlandjura",
        description: "Place"
    },
    {
        text: "England--Aylesford",
        href: "place/englandaylesford",
        description: "Place"
    },
    {
        text: "Scotland--Island of Iona",
        href: "place/scotlandisland_of_iona",
        description: "Place"
    },
    {
        text: "Scotland--Island of Skye",
        href: "place/scotlandisland_of_skye",
        description: "Place"
    },
    {
        text: "England--Melton Mowbray",
        href: "place/englandmelton_mowbray",
        description: "Place"
    },
    {
        text: "England--Stonehenge",
        href: "place/englandstonehenge",
        description: "Place"
    },
    {
        text: "Scotland--Culloden",
        href: "place/scotlandculloden",
        description: "Place"
    },
    {
        text: "Florida--Lake Buena Vista",
        href: "place/floridalake_buena_vista",
        description: "Place"
    },
    {
        text: "England--Isle of Purbeck",
        href: "place/englandisle_of_purbeck",
        description: "Place"
    },
]