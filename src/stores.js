import { writable } from "svelte/store";

export const pageTitle = writable('Postcards!')
export const top = writable(false)